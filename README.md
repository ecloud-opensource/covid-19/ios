# Covid-19

## Technology Stack

- MVP
- Lenguage: Swift
- UI: Storyboard

## Dependencies

- [github.com/ykyouhei/KYDrawerController](https://github.com/ykyouhei/KYDrawerController)
- [github.com/Alamofire/Alamofire](https://github.com/Alamofire/Alamofire)
- [github.com/onevcat/Kingfisher](https://github.com/onevcat/Kingfisher)
- [github.com/mac-cain13/R.swift](https://github.com/mac-cain13/R.swift)
- [github.com/chronotruck/FlagPhoneNumber](https://github.com/chronotruck/FlagPhoneNumber)
- [github.com/cbpowell/MarqueeLabel](https://github.com/cbpowell/MarqueeLabel)
- [github.com/apasccon/SearchTextField](https://github.com/apasccon/SearchTextField)
- [github.com/airbnb/lottie-ios](https://github.com/airbnb/lottie-ios)
- [github.com/firebase/firebase-ios-sdk](https://github.com/firebase/firebase-ios-sdk)
    - Core
    - Messaging
    - RemoteConfig
    - Crashlytics
    - Analytics
- [gitlab.com/jpcrespi/swift-extension](https://gitlab.com/jpcrespi/swift-extension)
