//
//  DefaultTests.swift
//  Covid-19-Tests
//
//  Created by Juan Pablo on 30/03/2020.
//  Copyright © 2020 eCloudSolutions. All rights reserved.
//

import XCTest

class DefaultTests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testDefault() throws {
        let deviceToken = UUID().uuidString
        
        UTNotificationDefault.deviceToken.remove()
        XCTAssertFalse(UTNotificationDefault.deviceToken.exists(), "")
        
        let deviceTokenNil1: String? = try? UTNotificationDefault.deviceToken.get()
        XCTAssertNil(deviceTokenNil1, "")
        
        UTNotificationDefault.deviceToken.set(token: deviceToken)
        XCTAssertNotNil(try? UTNotificationDefault.deviceToken.get(), "")
        XCTAssertEqual(deviceToken, try? UTNotificationDefault.deviceToken.get(), "")
        XCTAssertTrue(UTNotificationDefault.deviceToken.exists(), "")
        
        UTNotificationDefault.deviceToken.remove()
        XCTAssertFalse(UTNotificationDefault.deviceToken.exists(), "")
        
        let deviceTokenNil2: String? = try? UTNotificationDefault.deviceToken.get()
        XCTAssertNil(deviceTokenNil2, "")
    }
}
