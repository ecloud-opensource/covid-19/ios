//
//  ServiceTests.swift
//  Covid-19-Tests
//
//  Created by Juan Pablo on 29/03/2020.
//  Copyright © 2020 eCloudSolutions. All rights reserved.
//

import XCTest

class ServiceTests: XCTestCase {

    let timeout: Double = 300
    
    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        DLDefault.userToken.set(token: "eyJ0b2tlbiI6ImV5SjBlWEFpT2lKS1YxUWlMQ0poYkdjaU9pSklVekkxTmlKOS5leUpwWkNJNk1pd2lZM0psWVhSbFpFRjBJam9pTWpBeU1DMHdNeTB6TVZReU1EbzBNam95T1M0M05ETmFJaXdpZFhCa1lYUmxaRUYwSWpvaU1qQXlNQzB3TkMwd01sUXdNVG96TlRveU5TNDRNVFZhSWl3aWFYTkVaV3hsZEdWa0lqcG1ZV3h6WlN3aVptbHljM1JPWVcxbElqcHVkV3hzTENKc1lYTjBUbUZ0WlNJNmJuVnNiQ3dpWlcxaGFXd2lPaUl6TkRnNVEwUTRRUzB6TTBNNUxUUTBNek10UWpVd015MDRSRGxGTnpBMU5rWTRSak5BYldGcGJDNWpiMjBpTENKcFpHVnVkR2xtYVdOaGRHbHZiaUk2SWpNME9EZzBOall4SWl3aWNHaHZibVVpT2lJck5UUXpORFkyTkRBME5EQTNJaXdpWVdSa2NtVnpjeUk2Ym5Wc2JDd2liblZ0WW1WeUlqcHVkV3hzTENKellXWmxhRzkxYzJVaU9tNTFiR3dzSW1selVYVmhjbUZ1ZEdsdVpXUWlPbVpoYkhObExDSm1hVzVwYzJoUmRXRnlZVzUwYVc1bElqcG1ZV3h6WlN3aWNYVmhjbUZ1ZEdsdVpVWnliMjBpT201MWJHd3NJbkYxWVhKaGJuUnBibVZVYnlJNmJuVnNiQ3dpWjJWdVpHVnlJam9pVGtVaUxDSmhkbUYwWVhKVmNtd2lPbTUxYkd3c0luQnlaV2R1WVc1MElqcG1ZV3h6WlN3aWIyNU5iM1psYldWdWRDSTZabUZzYzJVc0ltOTFkRTltVTJGbVpXaHZkWE5sSWpwbVlXeHpaU3dpY0hKdlltRmlhV3hwZEhraU9tNTFiR3dzSW5OMFlYUjFjeUk2Ym5Wc2JDd2lhV1JEYVhSNUlqcHVkV3hzTENKcFpFNWxhV2RvWW05eWFHOXZaQ0k2Ym5Wc2JDd2lhV1JEYjNWdWRISjVJanB1ZFd4c0xDSnBaRnB2Ym1VaU9tNTFiR3dzSW1sa1RHRjBaWE4wVUc5emFYUnBiMjRpT2pjc0luUjVjR1VpT2lKMWMyVnlJbjAuSTFKQXFGNjFSTkdEYVdfOGpwcDg1ZkoyRG9wM21iWDZ6cTlPSXFickZkcyIsImV4cGlyYXRpb24iOjE2MTc0NjI1MDd9")
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        DLDefault.userToken.remove()
    }
    
    func testPostPhoneValidationRequest() throws {
        let expectation = self.expectation(description: "testPostPhoneValidationRequest")
        let data = PhoneValidationModel()
        
        data.phone = "+543466404407"
        data.identification = "34884661"
        
        let request = PostPhoneValidationRequest(data: data)
        request.performRequest {
            request.performResponse { response in
                XCTAssertTrue(response.status, response.message ?? "")
                expectation.fulfill()
            }
        }
        
        wait(for: [expectation], timeout: timeout)
    }
    
    func testPostCodeValidationRequest() throws {
        let expectation = self.expectation(description: "testPostCodeValidationRequest")
        let data = PhoneValidationModel()
        
        data.phone = "+543466404407"
        data.code = "814493"
        
        let request = PostCodeValidationRequest(data: data)
        request.performRequest {
            request.performResponse { response in
                XCTAssertTrue(response.status, response.message ?? "")
                XCTAssertNotNil(response.data?.user, "")
                XCTAssertNotNil(response.data?.token, "")
                XCTAssertEqual(response.data?.user?.phone, "+543466404407", "")
                XCTAssertEqual(response.data?.user?.identification, "34884661", "")
                expectation.fulfill()
            }
        }
        
        wait(for: [expectation], timeout: timeout)
    }
    
    func testGetUserMeRequest() throws {
        let expectation = self.expectation(description: "testGetUserMeRequest")
        
        let request = GetUserMeRequest()
        request.performRequest {
            request.performResponse { response in
                XCTAssertTrue(response.status, response.message ?? "")
                XCTAssertNotNil(response.data, "")
                XCTAssertEqual(response.data?.phone, "+543466404407", "")
                XCTAssertEqual(response.data?.identification, "34884661", "")
                expectation.fulfill()
            }
        }
        
        wait(for: [expectation], timeout: timeout)
    }
    
    func testPutUserMeRequest() throws {
        let expectation = self.expectation(description: "testGetUserMeRequest")
        let data = UserModel()
        let email = "\(UUID().uuidString)@mail.com"
        
        data.phone = "+543466404407"
        data.identification = "34884661"
        data.email = email
        data.safehouse = PointModel(latitude: -32.946828, longitude: -60.650995)
        
        let request = PutUserMeRequest(data: data)
        request.performRequest {
            request.performResponse { response in
                XCTAssertTrue(response.status, response.message ?? "")
                XCTAssertNotNil(response.data?.user, "")
                XCTAssertEqual(response.data?.user?.phone, "+543466404407", "")
                XCTAssertEqual(response.data?.user?.identification, "34884661", "")
                XCTAssertEqual(response.data?.user?.email, email, "")
                expectation.fulfill()
            }
        }
        
        wait(for: [expectation], timeout: timeout)
    }
    
    func testGetUserNotificationRequest() throws {
        let expectation = self.expectation(description: "testGetUserNotificationRequest")
        
        let request = GetUserNotificationRequest()
        request.performRequest {
            request.performResponse { response in
                XCTAssertTrue(response.status, response.message ?? "")
                XCTAssertNotNil(response.data, "")
                expectation.fulfill()
            }
        }
        
        wait(for: [expectation], timeout: timeout)
    }
    
    func testPostUserContactRequest() throws {
        let expectation = self.expectation(description: "testPostUserContactRequest")
        
        let data = ContactModel()
        data.firstName = "Juan"
        data.lastName = "Pablo"
        data.email = "juanpablocrespi99@gmail.com"
        data.message = "Holis"

        let request = PostUserContactRequest(data: data)
        request.performRequest {
            request.performResponse { response in
                XCTAssertTrue(response.status, response.message ?? "")
                XCTAssertNotNil(response.data, "")
                expectation.fulfill()
            }
        }
        
        wait(for: [expectation], timeout: timeout)
    }
    
    func testPostGeoNearbyRequest() throws {
        let expectation = self.expectation(description: "testPostGeoNearbyRequest")
        let data = BoundsModel()
        
        data.topLeft = PointModel(latitude: -32.9378976012659, longitude: -60.64517766237259)
        data.topRight = PointModel(latitude: -32.9378976012659, longitude: -60.62278926372528)
        data.bottomLeft = PointModel(latitude: -32.97129522779797, longitude: -60.64517766237259)
        data.bottomRight = PointModel(latitude: -32.97129522779797, longitude: -60.62278926372528)
        
        let request = PostGeoNearbRequest(data: data)
        request.performRequest {
            request.performResponse { response in
                XCTAssertTrue(response.status, response.message ?? "")
                expectation.fulfill()
            }
        }
        
        wait(for: [expectation], timeout: timeout)
    }
    
    func testPostGeoNewPointRequest() throws {
        let expectation = self.expectation(description: "testPostGeoNewPointRequest")
        let data = NewPointModel()
        
        data.point = PointModel(latitude: -32.946782, longitude: -60.651059)
        
        let request = PostGeoNewPointRequest(data: data)
        request.performRequest {
            request.performResponse { response in
                XCTAssertTrue(response.status, response.message ?? "")
                XCTAssertEqual(response.data?.point?.position?.coordinates?[0], -32.946782, "")
                XCTAssertEqual(response.data?.point?.position?.coordinates?[1], -60.651059, "")
                expectation.fulfill()
            }
        }
        
        wait(for: [expectation], timeout: timeout)
    }
    
    func testGetCountriesRequest() throws {
        let expectation = self.expectation(description: "testGetCountriesRequest")
        
        let request = GetCountriesRequest()
        request.performRequest {
            request.performResponse { response in
                XCTAssertTrue(response.status, response.message ?? "")
                XCTAssertNotNil(response.data, "")
                expectation.fulfill()
            }
        }
        
        wait(for: [expectation], timeout: timeout)
    }
    
    func testGetCityFeedRequest() throws {
        let expectation = self.expectation(description: "testGetCityFeedRequest")
        
        let request = GetCityFeedRequest()
        request.performRequest {
            request.performResponse { response in
                XCTAssertTrue(response.status, response.message ?? "")
                XCTAssertNotNil(response.data, "")
                expectation.fulfill()
            }
        }
        
        wait(for: [expectation], timeout: timeout)
    }

    func testGetCityLinksRequest() throws {
        let expectation = self.expectation(description: "testGetCityLinksRequest")
        
        let request = GetCityLinksRequest()
        request.performRequest {
            request.performResponse { response in
                XCTAssertTrue(response.status, response.message ?? "")
                XCTAssertNotNil(response.data, "")
                expectation.fulfill()
            }
        }
        
        wait(for: [expectation], timeout: timeout)
    }
    
    func testGetCitySingleRequest() throws {
        let expectation = self.expectation(description: "testGetCitySingleRequest")
        
        let request = GetCitySingleRequest()
        request.performRequest {
            request.performResponse { response in
                XCTAssertTrue(response.status, response.message ?? "")
                XCTAssertNotNil(response.data, "")
                expectation.fulfill()
            }
        }
        
        wait(for: [expectation], timeout: timeout)
    }
    
    func testGetNeighborhoodRankingRequest() throws {
        let expectation = self.expectation(description: "testGetNeighborhoodRankingRequest")
        
        let request = GetNeighborhoodRankingRequest()
        request.performRequest {
            request.performResponse { response in
                XCTAssertTrue(response.status, response.message ?? "")
                XCTAssertNotNil(response.data, "")
                expectation.fulfill()
            }
        }
        
        wait(for: [expectation], timeout: timeout)
    }
    
    func testGetSettingRequest() throws {
        let expectation = self.expectation(description: "testGetSettingRequest")

        let request = GetSettingRequest()
        request.performRequest {
            request.performResponse { response in
                XCTAssertTrue(response.status, response.message ?? "")
                XCTAssertNotNil(response.data, "")
                SettingManager.instance.write(objects: response.data?.rows ?? []) { _ in
                    SettingManager.instance.read(delegate: nil)
                    expectation.fulfill()
                }
            }
        }
        
        wait(for: [expectation], timeout: timeout)
    }
    
    func testGetTermsAndConditionsRequest() throws {
        let expectation = self.expectation(description: "testGetTermsAndConditionsRequest")

        let request = GetTermsAndConditionsRequest()
        request.performRequest {
            request.performResponse { response in
                XCTAssertTrue(response.status, response.message ?? "")
                XCTAssertNotNil(response.data, "")
            }
        }
        
        wait(for: [expectation], timeout: timeout)
    }
}
