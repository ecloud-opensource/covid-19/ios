//
//  ModelManagerTest.swift
//  Covid-19-Tests
//
//  Created by Juan Pablo on 29/03/2020.
//  Copyright © 2020 eCloudSolutions. All rights reserved.
//

import XCTest

class TestModel: NSObject, Codable {
    
    var value: String
    var numbers: [Int]?
    var staticString: String
    
    init(withValue value: String, numbers: [Int]? = nil) {
        self.value = value
        self.numbers = numbers
        self.staticString = "staticString"
        super.init()
    }
}

class DataTests: XCTestCase {
    

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testModel() throws {
        let filename = UUID().uuidString
        let value = UUID().uuidString
        let expectation = self.expectation(description: "testModel")
        DLDataManager.instance.remove(filename: filename) { _ in
            DLDataManager.instance.read(filename: filename) { (object: TestModel?, error: NSError?) in
                XCTAssertNotNil(error, "")
                XCTAssertEqual(error?.localizedDescription, "File Does Not Exist", "")
                XCTAssertNil(object, "")
                
                DLDataManager.instance.write(filename: filename, object: TestModel(withValue: value)) { error in
                    XCTAssertNil(error, "")

                    DLDataManager.instance.read(filename: filename) { (object: TestModel?, error: NSError?) in
                        XCTAssertNil(error, "")
                        XCTAssertNotNil(object, "")
                        XCTAssertEqual(object?.value, value, "")
                        
                        DLDataManager.instance.remove(filename: filename) { error in
                            XCTAssertNil(error, "")
                            
                            DLDataManager.instance.remove(filename: filename) { error in
                                XCTAssertNotNil(error, "")
                                XCTAssertEqual(error?.localizedDescription, "File Does Not Exist", "")
                                
                                DLDataManager.instance.read(filename: filename) { (object: TestModel?, error: NSError?) in
                                    XCTAssertNotNil(error, "")
                                    XCTAssertEqual(error?.localizedDescription, "File Does Not Exist", "")
                                    XCTAssertNil(object, "")
                                    expectation.fulfill()
                                }
                            }
                        }
                    }
                }
            }
        }
        
        wait(for: [expectation], timeout: 300)
    }
    
    func testCodableToDictionary() {
        let value = UUID().uuidString
        let numbers = [1,2,3,4,5]
        let model = TestModel(withValue: value, numbers: numbers)
        let dictionary = try? model.dictionary()
        XCTAssertNotNil(dictionary, "")
        XCTAssertEqual(dictionary?["value"] as? String, value, "")
        XCTAssertEqual(dictionary?["numbers"] as? [Int], numbers, "")
    }
}
