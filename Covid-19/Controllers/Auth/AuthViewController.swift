//
//  AuthViewController.swift
//  Covid-19
//
//  Created by Gonzalo Sanchez on 30/03/2020.
//  Copyright © 2020 eCloudSolutions. All rights reserved.
//

import UIKit

import UIKit
import SwiftExtension

class AuthNavigationController: UINavigationController, AuthPresenterDelegate {

    var authPresenter = AuthPresenter<AuthNavigationController>()

    override func viewDidLoad() {
        super.viewDidLoad()
        authPresenter.attach(view: self)
    }
}

class AuthBaseViewController: COVIDBaseViewController {
    var presenter: AuthPresenter<AuthNavigationController>? {
        return (navigationController as? AuthNavigationController)?.authPresenter
    }
}

class AuthViewController: AuthBaseViewController {
    
    @IBOutlet private var collectionView: UICollectionView!
        
    var delegate : RootViewControllerDelegate?
    var controllerType = ReusableControllerType.undismisssable

    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        checkControllerType()
    }
    
    
    // MARK: - Functions
    
    private func setup() {
        view.backgroundColor = .white
    }
    
    private func checkControllerType() {
        switch controllerType {
        case .drawer:
            navigationController?.navigationBar.setNavigationBarTranslucent()
            navigationItem.setLeftMenuButton(viewController: self)
            setOpenDrawerGestureModeMaskAll()
            interactivePopGestureRecognizer(false)
        case .helper:
            navigationController?.navigationBar.setNavigationBarTranslucent()
            navigationItem.setCloseMenuButton(viewController: self)
            setOpenDrawerGestureModeMaskNone()
            interactivePopGestureRecognizer(false)
        case .undismisssable:
            navigationController?.setNavigationBarHidden(true, animated: true)
            setOpenDrawerGestureModeMaskNone()
            interactivePopGestureRecognizer(false)
        }
    }
    
}

extension AuthViewController: UICollectionViewDataSource, UICollectionViewDelegate {
    
    // MARK: - UIScrollViewDelegate
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
    }
    
    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
        
    }
    
    // MARK: - UICollectionViewDataSource
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        return UICollectionViewCell()
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    // MARK: - UICollectionViewDelegateFlowLayout
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let height = collectionView.frame.height
        return CGSize(width: height, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.zero
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        return CGSize()
    }
    
    // MARK: - UICollectionViewDelegate
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }

}

