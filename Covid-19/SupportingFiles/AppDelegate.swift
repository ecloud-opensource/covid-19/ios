//
//  AppDelegate.swift
//  Covid-19
//
//  Created by Juan Pablo on 28/03/2020.
//  Copyright © 2020 eCloudSolutions. All rights reserved.
//

import UIKit
import SwiftExtension

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        configure(application: application, launchOptions: launchOptions)
        return true
    }
}

extension AppDelegate {
    
    func applicationWillResignActive(_ application: UIApplication) {
        
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        NotificationCenter.default.post(name: UINotificationName.applicationDidBecomeActive.value, object: nil, userInfo: nil)
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        
    }
    
}

extension AppDelegate {
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        SEMessaging.instance.did(registerForRemoteNotificationsWithDeviceToken: deviceToken)
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        SEMessaging.instance.did(failToRegisterForRemoteNotificationsWithError: error)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        SEMessaging.instance.did(receiveRemoteNotification: userInfo)
    }
}

