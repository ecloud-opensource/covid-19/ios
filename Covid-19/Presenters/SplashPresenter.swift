//
//  SplashPresenter.swift
//  Covid-19
//
//  Created by Gonzalo Sanchez on 01/04/2020.
//  Copyright © 2020 eCloudSolutions. All rights reserved.
//

import Foundation
import SwiftExtension

protocol SplashPresenterDelegate: BasePresenterDelegate {
    
}

class SplashPresenter<T: SplashPresenterDelegate>: BasePresenter<T> {

    
}
