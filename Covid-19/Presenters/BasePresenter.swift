//
//  BasePresenter.swift
//  Covid-19
//
//  Created by Juan Pablo on 28/03/2020.
//  Copyright © 2020 eCloudSolutions. All rights reserved.
//

import Foundation
import SwiftExtension

protocol BasePresenterDelegate: SENetworkDelegate  {
    
    func onError(message: String?)
    func initService(type: DLService)
    func finishService(type: DLService)
}

extension BasePresenterDelegate {
    
    func log(type: String, message: String) {
        #if DEBUG
        NSLog("[NETWORK][\(type)] \(message)")
        #endif
    }
    func onError(message: String?) {
        if let message = message {
            log(type: "ERROR", message: message)
        }
    }
    func initService(type: DLService) {
        log(type: "START", message: type.rawValue)
    }
    func finishService(type: DLService) {
        log(type: "END", message: type.rawValue)
    }
    func onError() {
        onError(message: "Error!")
    }
    func initService(name: String) {
        if let type = DLService(rawValue: name) {
            initService(type: type)
        }
    }
    func finishService(name: String) {
        if let type = DLService(rawValue: name) {
            finishService(type: type)
        }
    }
}

class BasePresenter<T: BasePresenterDelegate>: NSObject {
    
    internal var delegate: T?
    
    init(view: T? = nil) {
        delegate = view
    }
    
    func attach(view: T) {
        delegate = view
    }
    
    func detach() {
        delegate = nil
    }
}

