//
//  HomePresenter.swift
//  Covid-19
//
//  Created by Gonzalo Sanchez on 01/04/2020.
//  Copyright © 2020 eCloudSolutions. All rights reserved.
//

import UIKit
import CoreLocation
import SwiftExtension

protocol HomePresenterDelegate: BasePresenterDelegate {
    
}

extension HomePresenterDelegate {
    
}

class HomePresenter<T: HomePresenterDelegate>: BasePresenter<T> {
    
    var infoTableDatasource: [HomeUtilsCellType] {
        return [.flap, .info, .telephones, .news, .empty]
    }
    
    func openSMODSettings() {
        guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
            return
        }
        if UIApplication.shared.canOpenURL(settingsUrl) {
            UIApplication.shared.open(settingsUrl, completionHandler: nil)
        }
    }
    func getShareItems() -> [Any] {
        var items = [Any]()
        items.append(R.localizable.downloadCobeat())
        if let url = URL(string: HardCoded.landingURL.value), UIApplication.shared.canOpenURL(url) {
            items.append(R.localizable.downloadFrom("\(url)"))
        }
        return items
    }
    
    func callSympton() {
        #warning("Tomar de servicio")
        guard let number = URL(string: "tel://\(HardCoded.emergencyNumber.value)") else {
            return
        }
        UIApplication.shared.open(number, options: [:], completionHandler: nil)
    }
    
    func callReport() {
        #warning("Tomar de servicio")
        guard let number = URL(string: "tel://\(HardCoded.reportNumber.value)") else {
            return
        }
        UIApplication.shared.open(number, options: [:], completionHandler: nil)
    }
    
}



