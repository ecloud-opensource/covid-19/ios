//
//  PlacePresenter.swift
//  Covid-19
//
//  Created by Juan Pablo on 02/04/2020.
//  Copyright © 2020 eCloudSolutions. All rights reserved.
//

import Foundation
import SwiftExtension

protocol PlacePresenterDelegate: BasePresenterDelegate {
    func onGetCountries(_ countries: [CountryModel])
    func onGetCity(feed: [CityFeedModel])
    func onGetCityLink(city: CityModel, country: CountryModel)
    func onGetCitySingle(city: CityModel)
    func onGetNeighborhood(ranking: [RankingModel])
    func onGetCity(ranking: [RankingModel])
}

extension PlacePresenterDelegate {
    func onGetCountries(_ countries: [CountryModel]) { }
    func onGetCity(feed: [CityFeedModel]) { }
    func onGetCityLink(city: CityModel, country: CountryModel) { }
    func onGetCitySingle(city: CityModel) { }
    func onGetNeighborhood(ranking: [RankingModel]) { }
    func onGetCity(ranking: [RankingModel]) { }
}

class PlacePresenter<T: PlacePresenterDelegate>: BasePresenter<T> {
    
    var currentUserCountry: String {
        return UserManager.instance.user?.country?.name ?? R.localizable.yourCountry()
    }
    
    var currentUserCityID: String {
        guard let id = UserManager.instance.user?.city?.id else {
            return ""
        }
        return "\(id)"
    }
    
    var currentUserCityName: String {
        return UserManager.instance.user?.city?.name ?? R.localizable.yourCity()
    }
    func getCountries() {
        PlaceManager.instance.getCountries(delegate: delegate)
    }
    
    func getCityFeed() {
        PlaceManager.instance.getCityFeed(delegate: delegate)
    }
    
    func getCityLinks() {
        PlaceManager.instance.getCityLinks(delegate: delegate)
    }
    
    func getCitySingle(withID id: String? = nil) {
        let currentId: String = id ?? currentUserCityID
        PlaceManager.instance.getCitySingle(delegate: delegate, city: currentId)
    }
    
    func getNeighborhoodRanking() {
        PlaceManager.instance.getNeighborhoodRanking(delegate: delegate)
    }

    func getCityRanking(forCountry country: String? = nil) {
        PlaceManager.instance.getCityRanking(delegate: delegate, country: country)
    }
}
