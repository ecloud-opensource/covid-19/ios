//
//  RootPresenter.swift
//  Covid-19
//
//  Created by Gonzalo Sanchez on 30/03/2020.
//  Copyright © 2020 eCloudSolutions. All rights reserved.
//

import Foundation
import SwiftExtension

protocol RootPresenterDelegate: BasePresenterDelegate {
    func onReadUser()
    func onLogoutUser()
    func onSettings()
}

extension RootPresenterDelegate {
    func onReadUser() { }
    func onLogoutUser() { }
    func onSettings() { }
}

class RootPresenter<T: RootPresenterDelegate>: BasePresenter<T> {
    
    var logged: Bool {
        return user != nil
    }
    
    var user: UserModel? {
        return UserManager.instance.user
    }
    
    var settings: [SettingModel]? {
        return SettingManager.instance.settings
    }
    
    func readUser() {
        UserManager.instance.read(delegate: delegate)
    }
    
    func logout() {
        UserManager.instance.logout(delegate: delegate)
    }
    
    func readSettings() {
        SettingManager.instance.read(delegate: delegate)
    }
}


