//
//  SettingPresenter.swift
//  Covid-19
//
//  Created by Juan Pablo on 07/04/2020.
//  Copyright © 2020 eCloudSolutions. All rights reserved.
//

import Foundation
import SwiftExtension

protocol SettingPresenterDelegate: BasePresenterDelegate {
    func onSettings()
    func onTermsAndConditions(url: String)
}

extension SettingPresenterDelegate {
    func onSettings() { }
    func onTermsAndConditions(url: String) { }
}

class SettingPresenter<T: SettingPresenterDelegate>: BasePresenter<T> {
    
    func getSettings() {
        SettingManager.instance.getSettings(delegate: delegate)
    }
    
    func getTermsAndConditions() {
        SettingManager.instance.getTermsAndConditions(delegate: delegate)
    }
}
