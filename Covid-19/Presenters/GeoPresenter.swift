//
//  GeoPresenter.swift
//  Covid-19
//
//  Created by Juan Pablo on 30/03/2020.
//  Copyright © 2020 eCloudSolutions. All rights reserved.
//

import Foundation
import SwiftExtension
import CoreLocation

protocol GeoPresenterDelegate: BasePresenterDelegate {
    func onPostGeo(nearby users: [UserModel], notifications: [NotificationModel])
    func onPostGeo(newPoint position: PositionModel)
}

extension GeoPresenterDelegate {
    func onPostGeo(nearby users: [UserModel], notifications: [NotificationModel]) { }
    func onPostGeo(newPoint position: PositionModel) { }
}

class GeoPresenter<T: GeoPresenterDelegate>: BasePresenter<T> {
    
    var locationAvailable: Bool {
        return SELocation.instance.authorized
    }
    
    override func attach(view: T) {
        super.attach(view: view)
    }
    
    // MARK: - Services

    func postGeoNearby(data: BoundsModel) {
        GeoManager.instance.postGeoNearby(delegate: delegate, data: data)
    }
    
    func postGeoNewPoint(data: NewPointModel) {
        GeoManager.instance.postGeoNewPoint(delegate: delegate, data: data)
    }
}
