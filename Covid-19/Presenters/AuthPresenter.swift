//
//  AuthPresenter.swift
//  Covid-19
//
//  Created by Juan Pablo on 29/03/2020.
//  Copyright © 2020 eCloudSolutions. All rights reserved.
//

import Foundation
import SwiftExtension

protocol AuthPresenterDelegate: BasePresenterDelegate {
    func updateSMSTimer(_ value: String?)
    func onPostPhoneValidation()
    func onPostCodeValidation()
    func onGetUserMe()
    func onPutUserMe()
}

extension AuthPresenterDelegate {
    func updateSMSTimer(_ value: String?) { }
    func onPostPhoneValidation() { }
    func onPostCodeValidation() { }
    func onGetUserMe() { }
    func onPutUserMe() { }
}

class AuthPresenter<T: AuthPresenterDelegate>: BasePresenter<T> {
    
    private var termsURL: String {
        return (try? UIDefault.termsURL.get()) ?? HardCoded.landingURL.value
    }
    
    // MARK: - Timer var
    
    private var SMSTimer: Timer?
    var getSMSTimerIsActive: Bool {
        if let _ : String = try? UIDefault.SMSTimer.get() {
            if (Date().timeIntervalSince1970 - getSMSTimerValue.timeIntervalSince1970) >= getSMSMaxTime {
                stopSMSTimer()
                return false
            }
            return true
        }
        return false
    }
    var getSMSTimerIsSameNumber: Bool {
        if let oldPhone: String = try? UIDefault.SMSTimerLastNumber.get(), !oldPhone.isEmpty, oldPhone == phoneValidationModel.phone {
            return true
        }
        return false
    }
    private var getSMSTimerValue: Date {
        if let interval: String = try? UIDefault.SMSTimer.get(), let double = Double(interval) {
            return Date(timeIntervalSince1970: double)
        }
        return Date()
    }
    var getSMSRemainingTime: Int {
        return Int(getSMSMaxTime - getSMSElapsedTime)
    }
    private var getSMSElapsedTime: Double {
        return Date().timeIntervalSince1970 - getSMSTimerValue.timeIntervalSince1970
    }
    private var getSMSMaxTime: Double {
        if let value = SettingManager.instance.setting(code: .SMSResendTime)?.value {
            if let double = NumberFormatter().number(from: value)?.doubleValue {
                return double
            }
        }
        return 60.0
    }
    
    // MARK: - Data var
    
    private var phoneValidationModel = PhoneValidationModel()
    var enteredPhone: String {
        return phoneValidationModel.phone ?? ""
    }
    // MARK: - Functions
    
    func showTerms() {
        COBEATUIUtils.openInternalBrowser(withStringURL: termsURL)
    }
    
    func set(phone: String?) {
        guard let phoneOpt = phone else {
            delegate?.onError(message: R.localizable.checkEnteredData())
            return
        }
        phoneValidationModel.phone = phoneOpt
        if getSMSTimerIsActive == true {
            if getSMSTimerIsSameNumber == true {
                delegate?.onPostPhoneValidation()
            } else {
                delegate?.onError(message: R.localizable.mustWaitToResendCode("\(getSMSRemainingTime.formattedToMinutesAndSecondsFull)"))
            }
            return
        }
        postPhoneValidation(data: phoneValidationModel)
    }
    
    func set(code: String?) {
        guard let codeOpt = code else {
            delegate?.onError(message: R.localizable.checkEnteredData())
            return
        }
        phoneValidationModel.code = codeOpt
        postCodeValidation(data: phoneValidationModel)
    }
    
    func clearCode() {
        phoneValidationModel.code = nil
    }
    
    func resendCode() {
        postPhoneValidation(data: phoneValidationModel)
    }
    
    // MARK: - Functions Timer
    
    func startSMSTimer() {
        if !getSMSTimerIsActive || !getSMSTimerIsSameNumber {
            stopSMSTimer()
            UIDefault.SMSTimer.set(value: "\(Date().timeIntervalSince1970)")
            UIDefault.SMSTimerLastNumber.set(value: "\(phoneValidationModel.phone ?? "")")
        }
        SMSTimer?.invalidate()
        SMSTimer = nil
        SMSTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateSMSTimer), userInfo: nil, repeats: true)
        if let timer = SMSTimer { RunLoop.main.add(timer, forMode: RunLoop.Mode.common) }
    }
    
    private func stopSMSTimer() {
        UIDefault.SMSTimer.remove()
        UIDefault.SMSTimerLastNumber.remove()
        SMSTimer?.invalidate()
        SMSTimer = nil
    }
    
    @objc private func updateSMSTimer() {
        guard getSMSRemainingTime > 0 else {
            stopSMSTimer()
            delegate?.updateSMSTimer(nil)
            return
        }
        delegate?.updateSMSTimer(R.localizable.resendCodeRemaining("\(getSMSRemainingTime.formattedToMinutesAndSeconds)"))
    }
    
    // MARK: - Functions Services
    
    private func postPhoneValidation(data: PhoneValidationModel) {
        AuthManager.instance.postPhoneValidation(delegate: delegate, data: data)
    }
    
    private func postCodeValidation(data: PhoneValidationModel) {
        AuthManager.instance.postCodeValidation(delegate: delegate, data: data)
    }
}
