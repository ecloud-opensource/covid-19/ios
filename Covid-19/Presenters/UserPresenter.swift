//
//  UserPresenter.swift
//  Covid-19
//
//  Created by Juan Pablo on 29/03/2020.
//  Copyright © 2020 eCloudSolutions. All rights reserved.
//

import Foundation
import SwiftExtension
import CoreLocation
import GooglePlaces

protocol UserPresenterDelegate: BasePresenterDelegate {
    func onGetUserMe()
    func onPutUserMe()
    func onGetUser(notification: [NotificationModel])
    func onPostUserQuestionnaire()
    func onPostUserContact()
    func showTextfieldsErrors(_ textfields: [TextfieldType])
}

extension UserPresenterDelegate {
    func onGetUserMe() { }
    func onPutUserMe() { }
    func onGetUser(notification: [NotificationModel]) { }
    func onPostUserQuestionnaire() { }
    func onPostUserContact() { }
    func showTextfieldsErrors(_ textfields: [TextfieldType]) { }
}

class ProfileDatasource {
    var type: ProfileCellType
    var value: String?
    init(type: ProfileCellType, value: String? = nil) {
        self.type = type
        self.value = value
    }
}

class UserPresenter<T: UserPresenterDelegate>: BasePresenter<T> {
    
    var profileDatasource: [ProfileDatasource] = []
    var editingUser: UserModel?
    
    var getUserHasEnteredSafehouse: Bool {
        return UserManager.instance.user?.safehouse != nil
    }
    
    var getUserLocation: CLLocationCoordinate2D? {
        if let location = SELocation.instance.location.value {
            return CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
        }
        return nil
    }
    
    var getSafehouseLocation: CLLocationCoordinate2D? {
        if let safehouseCoord = UserManager.instance.user?.safehouse?.coordinates, safehouseCoord.count == 2 {
            return CLLocationCoordinate2D(latitude: safehouseCoord[0], longitude: safehouseCoord[1])
        }
        return nil
    }
    
    override func attach(view: T) {
        super.attach(view: view)
        fetchUserDatasource()
    }
    
    func fetchUserDatasource() {
        editingUser = UserModel()
        profileDatasource = []
        if let user = UserManager.instance.user {
            /// Only copy properties that can be edited
            editingUser?.firstName = user.firstName
            editingUser?.lastName = user.lastName
            editingUser?.email = user.email
            editingUser?.address = user.address
            editingUser?.number = user.number
            editingUser?.phone = user.phone
            editingUser?.identification = user.identification
            editingUser?.status = user.status /// Shouldn't be edited
        }
        profileDatasource.append(ProfileDatasource(type: .empty))
        profileDatasource.append(ProfileDatasource(type: .firstname, value: editingUser?.firstName))
        profileDatasource.append(ProfileDatasource(type: .lastname, value: editingUser?.lastName))
        profileDatasource.append(ProfileDatasource(type: .email, value: editingUser?.email))
        profileDatasource.append(ProfileDatasource(type: .address, value: editingUser?.fullAddress))
        profileDatasource.append(ProfileDatasource(type: .phone, value: editingUser?.phone))
        profileDatasource.append(ProfileDatasource(type: .identification, value: editingUser?.identification))
        profileDatasource.append(ProfileDatasource(type: .status, value: editingUser?.status))
        profileDatasource.append(ProfileDatasource(type: .footer))
    }
    
    func updateUseService() {
        guard let user = editingUser else {
            delegate?.onError(message: R.localizable.checkEnteredData())
            return
        }
        var errors = [TextfieldType]()
        if user.firstName?.isEmpty == true || user.firstName?.isOnlyText() == false {
            errors.append(.firstName)
        }
        if user.lastName?.isEmpty == true || user.lastName?.isOnlyText() == false {
            errors.append(.lastName)
        }
        if user.email?.isEmpty == true || user.email?.isValidEmail() == false {
            errors.append(.email)
        }
        if user.identification?.isEmpty == true || user.identification?.isValidNumber() == false {
            errors.append(.identification)
        }
        editingUser?.status = nil /// Shouldn't be edited
        putUserMe(data: user)
    }
    
    func getUserMe() {
        UserManager.instance.getUserMe(delegate: delegate)
    }
    
    func putUserMe(data: UserModel) {
        UserManager.instance.putUserMe(delegate: delegate, data: data)
    }
    
    func fetUserNotifications() {
        UserManager.instance.getUserNotification(delegate: delegate)
    }
    
    func postUserQuestionnaire() {
        UserManager.instance.postUserQuestionnaire(delegate: delegate)
    }
    
    func postUserContact(data: ContactModel) {
        UserManager.instance.postUserContact(delegate: delegate, data: data)
    }
}
