//
//  Notifications.swift
//  Covid-19
//
//  Created by Juan Pablo on 16/04/2020.
//  Copyright © 2020 eCloudSolutions. All rights reserved.
//

import Foundation
import SwiftExtension
import FirebaseMessaging

class FirebaseMessagingMananger: NSObject, SEObserver {
    
    var id = "notifications"
    
    class var shared : FirebaseMessagingMananger {
        struct Static {
            static let sharedManager = FirebaseMessagingMananger()
        }
        return Static.sharedManager
    }
    
    override init() {
        super.init()
        Messaging.messaging().delegate = self
    }
    
    func setup() {
        SEMessaging.instance.deviceToken.add(observer: self, on: didReceiveDeviceToken)
    }
    
    func didReceiveDeviceToken(token: Data?, error: NSError?) {
        guard let token = token else { return }
        Messaging.messaging().apnsToken = token
        did(receiveMessagingRegistrationToken: Messaging.messaging().fcmToken)
    }
    
    private func did(receiveMessagingRegistrationToken firebaseToken: String?) {
        SEMessaging.instance.did(receiveMessagingRegistrationToken: firebaseToken)
        Messaging.messaging().subscribe(toTopic: FirebaseMessagingTopic.general.rawValue)
        Messaging.messaging().subscribe(toTopic: FirebaseMessagingTopic.ios.rawValue)
        #if DEBUG
        Messaging.messaging().subscribe(toTopic: FirebaseMessagingTopic.test.rawValue)
        #endif
    }
}

extension FirebaseMessagingMananger: MessagingDelegate {

    public func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        did(receiveMessagingRegistrationToken: fcmToken)
    }
}
