//
//  BaseRequest.swift
//  Covid-19
//
//  Created by Juan Pablo on 08/04/2020.
//  Copyright © 2020 eCloudSolutions. All rights reserved.
//

import Foundation
import SwiftExtension

class BaseRespose<U: Codable>: NSObject, SEResponseDelegate {
    typealias W = U
    
    var status: Bool
    var message: String?
    var data: U?
    
    required override init() {
        self.status = true
    }
    
    required init(data: U) {
        self.status = true
        self.data = data
    }
    
    required init(error: NSError) {
        self.status = false
        self.message = error.localizedDescription
    }
}

class BaseRequest<T, U: Codable, V>: SENetwork<T, BaseRespose<U>, V>.Request {
 
    override func baseUrl() -> String {
        #warning("Handle produnction/staging environments")
        return "https://api.cobeat.app"
        //return "https://stg.backend.pandemia.ecloudsolutions.com"
    }
    
    override func headers() throws -> [String: String] {
        var headers = try super.headers()
        
        headers["device"] = "ios"
        headers["accept-language"] = DLLanguage.current().rawValue
        headers["x-api-key"] = try? DLDefault.userToken.get()
     
        return headers
    }
}
