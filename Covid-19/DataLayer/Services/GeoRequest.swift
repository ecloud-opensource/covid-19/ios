//
//  GeoRequest.swift
//  Covid-19
//
//  Created by Juan Pablo on 01/04/2020.
//  Copyright © 2020 eCloudSolutions. All rights reserved.
//

import Foundation
import SwiftExtension

class PostGeoNearbRequest: BaseRequest<BoundsModel, PostGeoNearbRequest.Data, Any> {
    
    class Data: NSObject, Codable {
        var users: [UserModel]?
        var notifications: [NotificationModel]?
    }
    
    override init(data: BoundsModel) {
        super.init(name: DLService.postGeoNearby.rawValue,
                   path: "/geo/nearby",
                   method: .post,
                   data: data)
    }
}

class PostGeoNewPointRequest: BaseRequest<NewPointModel, PostGeoNewPointRequest.Data ,Any> {
 
    class Data: NSObject, Codable {
        var point: PositionModel?
    }
    
    override init(data: NewPointModel) {
        super.init(name: DLService.postGeoNearby.rawValue,
                   path: "/geo/new-point",
                   method: .post,
                   data: data)
    }
}
