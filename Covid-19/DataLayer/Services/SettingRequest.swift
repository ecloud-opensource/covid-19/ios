//
//  SettingRequest.swift
//  Covid-19
//
//  Created by Juan Pablo on 07/04/2020.
//  Copyright © 2020 eCloudSolutions. All rights reserved.
//

import Foundation

class GetSettingRequest: BaseRequest<Void, GetSettingRequest.Data, Any> {

    class Data: NSObject, Codable {
        var metadata: MetadataModel?
        var rows: [SettingModel]?
    }
    
    init() {
        super.init(name: DLService.setting.rawValue,
                   path: "/setting",
                   method: .get)
    }
}

class GetTermsAndConditionsRequest: BaseRequest<Void, GetTermsAndConditionsRequest.Data, Any> {
    
    class Data: NSObject, Codable {
        var url: String?
    }
    
    init() {
        super.init(name: DLService.setting.rawValue,
                   path: "/setting/terms-and-conditions",
                   method: .get)
    }
}
