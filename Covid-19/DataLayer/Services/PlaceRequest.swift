//
//  PlaceRequest.swift
//  Covid-19
//
//  Created by Juan Pablo on 02/04/2020.
//  Copyright © 2020 eCloudSolutions. All rights reserved.
//

import Foundation
import SwiftExtension

class GetCountriesRequest: BaseRequest<GetCountriesRequest.Query, GetCountriesRequest.Data, Any> {

    class Data: NSObject, Codable {
        var metadata: MetadataModel?
        var rows: [CountryModel]?
    }
    
    class Query: QueryModel {
        convenience init() {
            self.init(scope: ["cities"])
        }
    }
    
    init() {
        super.init(name: DLService.getCountries.rawValue,
                   path: "/country/mobile",
                   method: .get,
                   data: Query())
    }
}

class GetCityFeedRequest: BaseRequest<Void, GetCityFeedRequest.Data, Any> {

    class Data: NSObject, Codable {
        var metadata: MetadataModel?
        var rows: [CityFeedModel]?
    }
    
    init() {
        super.init(name: DLService.getCityFeed.rawValue,
                   path: "/city/feed",
                   method: .get)
    }
}

class GetCityLinksRequest: BaseRequest<Void, GetCityLinksRequest.Data, Any> {
    
    class Data: NSObject, Codable {
        var city: CityModel?
        var country: CountryModel?
    }
    
    init() {
        super.init(name: DLService.getCityLinks.rawValue,
                   path: "/city/links",
                   method: .get)
    }
}

class GetCitySingleRequest: BaseRequest<Void, GetCitySingleRequest.Data, Any> {
    
    class Data: NSObject, Codable {
        var city: CityModel?
    }
    
    init(city id: String) {
        super.init(name: DLService.getCitySingle.rawValue,
                   path: "/city/mobile/\(id)",
                   method: .get)
    }   
}

class GetNeighborhoodRankingRequest: BaseRequest<Void, GetNeighborhoodRankingRequest.Data, Any> {
 
    class Data: NSObject, Codable {
        var metadata: MetadataModel?
        var rows: [RankingModel]?
    }
    
    init() {
        super.init(name: DLService.getNeighborhoodRanking.rawValue,
                   path: "/neighborhood/ranking",
                   method: .get)
    }
}

class GetCityRankingRequest: BaseRequest<GetCityRankingRequest.Query, GetCityRankingRequest.Data, Any> {
 
    class Data: NSObject, Codable {
        var metadata: MetadataModel?
        var rows: [RankingModel]?
    }
    
    class Query: QueryModel {
        
        var idCountry: String?
        
        convenience init(country id: String?) {
            self.init()
            self.pageSize = 200
            self.idCountry = id
        }
        
        private enum CodingKeys: String, CodingKey {
            case idCountry
        }
        
        override func encode(to encoder: Encoder) throws {
            try super.encode(to: encoder)
            var container = encoder.container(keyedBy: CodingKeys.self)
            try container.encode(self.idCountry, forKey: .idCountry)
        }
    }
    
    init(country id: String?) {
        super.init(name: DLService.getCityRanking.rawValue,
                   path: "/city/ranking",
                   method: .get,
                   data: Query(country: id))
    }
}
