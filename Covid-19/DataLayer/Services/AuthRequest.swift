//
//  AuthRequest.swift
//  Covid-19
//
//  Created by Juan Pablo on 01/04/2020.
//  Copyright © 2020 eCloudSolutions. All rights reserved.
//

import Foundation
import SwiftExtension

class PostPhoneValidationRequest: BaseRequest<PhoneValidationModel, PostPhoneValidationRequest.Data, Any> {

    class Data: NSObject, Codable {
        
    }
    
    override init(data: PhoneValidationModel) {
        super.init(name: DLService.postPhoneValidation.rawValue,
                   path: "/user/phone-validation",
                   method: .post,
                   data: data)
    }
}

class PostCodeValidationRequest: BaseRequest<PhoneValidationModel, PostCodeValidationRequest.Data, Any> {
 
    class Data: NSObject, Codable {
        var user: UserModel?
        var token: String?
    }
    
    override init(data: PhoneValidationModel) {
        super.init(name: DLService.postCodeValidation.rawValue,
                   path: "/user/code-validation",
                   method: .post,
                   data: data)
    }

    override func body() throws -> [String : AnyObject] {
        var body = try super.body()
        body?["firebaseToken"] = try SEMessagingDefault.firebase.get()
        return body ?? [:]
    }
    
}
