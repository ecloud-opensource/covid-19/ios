//
//  UserRequest.swift
//  Covid-19
//
//  Created by Juan Pablo on 01/04/2020.
//  Copyright © 2020 eCloudSolutions. All rights reserved.
//

import Foundation
import SwiftExtension

class GetUserMeRequest: BaseRequest<GetUserMeRequest.Query, GetUserMeRequest.Data, Any> {

    class Data: UserModel {
        
    }
    
    class Query: QueryModel {
        convenience init() {
            self.init(scope: ["country", "city", "neighborhood"])
        }
    }

    init() {
        super.init(name: DLService.getUserMe.rawValue,
                   path: "/user/me",
                   method: .get,
                   data: Query())
    }
}

class PutUserMeRequest: BaseRequest<UserModel, PutUserMeRequest.Data, Any> {
    
    class Data: NSObject, Codable {
        var user: UserModel?
    }
    
    override init(data: UserModel) {
        super.init(name: DLService.putUserMe.rawValue,
                   path: "/user/me",
                   method: .put,
                   data: data)
    }
}

class GetUserNotificationRequest: BaseRequest<Void, GetUserNotificationRequest.Data, Any> {
    
    class Data: NSObject, Codable {
        var notifications: [NotificationModel]?
    }
    
    init() {
        super.init(name: DLService.getUserNotification.rawValue,
                   path: "/user/notifications",
                   method: .get)
    }
}

class PostUserQuestionnaireRequest: BaseRequest<Void, PostUserQuestionnaireRequest.Data, Any> {
    
    class Data: NSObject, Codable {
        
    }
    
    init() {
        super.init(name: DLService.postUserQuestionnaire.rawValue,
                   path: "/user/questionnaire",
                   method: .post)
    }
}

class PostUserContactRequest: BaseRequest<ContactModel, PostUserContactRequest.Data, Any> {
    
    class Data: NSObject, Codable {
        
    }
    
    override init(data: ContactModel) {
        super.init(name: DLService.postUserContact.rawValue,
                   path: "/user/contact",
                   method: .post,
                   data: data)
    }
}
