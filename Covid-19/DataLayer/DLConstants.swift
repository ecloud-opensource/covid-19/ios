//
//  DLConstants.swift
//  Covid-19
//
//  Created by Juan Pablo on 30/03/2020.
//  Copyright © 2020 eCloudSolutions. All rights reserved.
//

import Foundation
import SwiftExtension

enum DLService: String {
    case postLogin
    case postPhoneValidation
    case postCodeValidation
    case getUserMe
    case putUserMe
    case getUserNotification
    case postUserQuestionnaire
    case postUserContact
    case postGeoNearby
    case postGeoNewPoint
    case getCountries
    case getCityFeed
    case getCityLinks
    case getCitySingle
    case getNeighborhoodRanking
    case getCityRanking
    case setting
}

enum DLDefault: String, SEDefaultDelegate {
    case userToken
}

enum DLLanguage: String {
    case english = "en"
    case spanish = "es"
    
    static func current() -> DLLanguage {
        return DLLanguage(optValue: Locale.preferredLanguages.first?.components(separatedBy: "-").first) ?? .spanish
    }
}
