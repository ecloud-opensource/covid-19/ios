//
//  PlaceManager.swift
//  Covid-19
//
//  Created by Juan Pablo on 02/04/2020.
//  Copyright © 2020 eCloudSolutions. All rights reserved.
//

import Foundation
import SwiftExtension

class PlaceManager: NSObject {
        
    class var instance: PlaceManager {
        struct Static {
            static let instance = PlaceManager()
        }
        return Static.instance
    }
    
    func getCountries(delegate: PlacePresenterDelegate?) {
        let request = GetCountriesRequest()
        request.performRequest(delegate: delegate) {
            request.performResponse { response in
                if response.status == false {
                    delegate?.onError(message: response.message)
                } else if let countries = response.data?.rows {
                    delegate?.onGetCountries(countries)
                } else {
                    delegate?.onError()
                }
            }
        }
    }
    
    func getCityFeed(delegate: PlacePresenterDelegate?) {
        let request = GetCityFeedRequest()
        request.performRequest(delegate: delegate) {
            request.performResponse { response in
                if response.status == false {
                    delegate?.onError(message: response.message)
                } else if let rows = response.data?.rows {
                    delegate?.onGetCity(feed: rows)
                } else {
                    delegate?.onError()
                }
            }
        }
    }
    
    func getCityLinks(delegate: PlacePresenterDelegate?) {
        let request = GetCityLinksRequest()
        request.performRequest(delegate: delegate) {
            request.performResponse { response in
                if response.status == false {
                    delegate?.onError(message: response.message)
                } else if let city = response.data?.city, let country = response.data?.country {
                    delegate?.onGetCityLink(city: city, country: country)
                } else {
                    delegate?.onError()
                }
            }
        }
    }
    
    func getCitySingle(delegate: PlacePresenterDelegate?, city id: String) {
        let request = GetCitySingleRequest(city: id)
        request.performRequest(delegate: delegate) {
            request.performResponse { response in
                if response.status == false {
                    delegate?.onError(message: response.message)
                } else if let city = response.data?.city {
                    delegate?.onGetCitySingle(city: city)
                } else {
                    delegate?.onError()
                }
            }
        }
    }
    
    func getNeighborhoodRanking(delegate: PlacePresenterDelegate?) {
        let request = GetNeighborhoodRankingRequest()
        request.performRequest(delegate: delegate) {
            request.performResponse { response in
                if response.status == false {
                    delegate?.onError(message: response.message)
                } else if let rows = response.data?.rows {
                    delegate?.onGetNeighborhood(ranking: rows)
                } else {
                    delegate?.onError()
                }
            }
        }
    }
    
    func getCityRanking(delegate: PlacePresenterDelegate?, country id: String?) {
        let request = GetCityRankingRequest(country: id)
        request.performRequest(delegate: delegate) {
            request.performResponse { response in
                if response.status == false {
                    delegate?.onError(message: response.message)
                } else if let rows = response.data?.rows {
                    delegate?.onGetCity(ranking: rows)
                } else {
                    delegate?.onError()
                }
            }
        }
    }
    
}

