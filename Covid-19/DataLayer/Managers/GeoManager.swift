//
//  GeoManager.swift
//  Covid-19
//
//  Created by Juan Pablo on 30/03/2020.
//  Copyright © 2020 eCloudSolutions. All rights reserved.
//

import Foundation
import SwiftExtension

class GeoManager: NSObject {
        
    class var instance: GeoManager {
        struct Static {
            static let instance = GeoManager()
        }
        return Static.instance
    }
    
    func postGeoNearby(delegate: GeoPresenterDelegate?, data: BoundsModel) {
        let request = PostGeoNearbRequest(data: data)
        request.performRequest(delegate: delegate) {
            request.performResponse { response in
                if response.status == false {
                    delegate?.onError(message: response.message)
                } else if let users = response.data?.users {
                    delegate?.onPostGeo(nearby: users, notifications: response.data?.notifications ?? [])
                } else {
                    delegate?.onError()
                }
            }
        }
    }
    
    func postGeoNewPoint(delegate: GeoPresenterDelegate?, data: NewPointModel) {
        let request = PostGeoNewPointRequest(data: data)
        request.performRequest(delegate: delegate) {
            request.performResponse { response in
                if response.status == false {
                    delegate?.onError(message: response.message)
                } else if let position = response.data?.point {
                    delegate?.onPostGeo(newPoint: position)
                } else {
                    delegate?.onError()
                }
            }
        }
    }
}
