//
//  AuthManager.swift
//  Covid-19
//
//  Created by Juan Pablo on 29/03/2020.
//  Copyright © 2020 eCloudSolutions. All rights reserved.
//

import Foundation
import SwiftExtension

class AuthManager: NSObject {
    
    class var instance: AuthManager {
        struct Static {
            static let instance = AuthManager()
        }
        return Static.instance
    }
    
    func postPhoneValidation(delegate: AuthPresenterDelegate?, data: PhoneValidationModel) {
        let request = PostPhoneValidationRequest(data: data)
        request.performRequest(delegate: delegate) {
            request.performResponse { response in
                if response.status == false {
                    delegate?.onError(message: response.message)
                } else {
                    delegate?.onPostPhoneValidation()
                }
            }
        }
    }
    
    func postCodeValidation(delegate: AuthPresenterDelegate?, data: PhoneValidationModel) {
        let request = PostCodeValidationRequest(data: data)
        request.performRequest(delegate: delegate) {
            request.performResponse { response in
                if response.status == false  {
                    delegate?.onError(message: response.message)
                } else if let token = response.data?.token, let user = response.data?.user {
                    DLDefault.userToken.set(value: token)
                    UserManager.instance.write(object: user) { error in
                        if let error = error {
                            delegate?.onError(message: error.localizedDescription)
                        } else {
                            delegate?.onGetUserMe()
                        }
                    }
                } else {
                    delegate?.onError()
                }
            }
        }
    }
}
