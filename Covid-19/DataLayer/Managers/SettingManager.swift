//
//  SettingManager.swift
//  Covid-19
//
//  Created by Juan Pablo on 07/04/2020.
//  Copyright © 2020 eCloudSolutions. All rights reserved.
//

import Foundation
import SwiftExtension

class SettingManager: NSObject {
    
    private let settingFilename = "settings"
        
    class var instance: SettingManager {
        struct Static {
            static let instance = SettingManager()
        }
        return Static.instance
    }
    
    var settings: [SettingModel]?
    
    func setting(code: SettingCode) -> SettingModel? {
        return settings?.filter({ $0.codeValue == code }).first
    }
    
    func write(objects: [SettingModel]?, completion: @escaping ((NSError?) -> Void)) {
        SECodable.instance.write(filename: settingFilename, object: objects) { error in
            if let error = error {
                completion(error)
            } else {
                self.settings = objects
                completion(nil)
            }
        }
    }
    
    func read(delegate: RootPresenterDelegate?) {
        SECodable.instance.read(filename: settingFilename) { (settings: [SettingModel]?, error: NSError?) in
            if let error = error {
                delegate?.onError(message: error.localizedDescription)
            } else {
                self.settings = settings
                delegate?.onSettings()
            }
        }
    }
    
    func getSettings(delegate: SettingPresenterDelegate?) {
        let request = GetSettingRequest()
        request.performRequest(delegate: delegate) {
            request.performResponse { response in
                if response.status == false {
                    delegate?.onError(message: response.message)
                } else if let rows = response.data?.rows {
                    self.write(objects: rows) { error in
                        if let error = error {
                            delegate?.onError(message: error.localizedDescription)
                        } else {
                            delegate?.onSettings()
                        }
                    }
                } else {
                    delegate?.onError()
                }
            }
        }
    }
    
    func getTermsAndConditions(delegate: SettingPresenterDelegate?) {
        let request = GetTermsAndConditionsRequest()
        request.performRequest(delegate: delegate) {
            request.performResponse { response in
                if response.status == false {
                    delegate?.onError(message: response.message)
                } else if let url = response.data?.url {
                    delegate?.onTermsAndConditions(url: url)
                } else {
                    delegate?.onError()
                }
            }
        }
    }
}
