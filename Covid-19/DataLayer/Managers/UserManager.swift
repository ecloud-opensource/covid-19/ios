//
//  UserManager.swift
//  Covid-19
//
//  Created by Juan Pablo on 29/03/2020.
//  Copyright © 2020 eCloudSolutions. All rights reserved.
//

import Foundation
import SwiftExtension
import CoreLocation

class UserManager: NSObject, SEObserver {
    
    var id = "user-mnager-observer"
    
    private let userFilename = "user"
    private var positionTime: Date?

    class var instance: UserManager {
        struct Static {
            static let instance = UserManager()
        }
        return Static.instance
    }
    
    var user: UserModel? {
        didSet {
            if user == nil {
                SELocation.instance.location.remove(observer: self)
            } else {
                SELocation.instance.location.add(observer: self, on: on)
            }
        }
    }

    func write(object: UserModel?, completion: @escaping ((NSError?) -> Void)) {
        SECodable.instance.write(filename: userFilename, object: object) { error in
            if let error = error {
                completion(error)
            } else {
                self.user = object
                completion(nil)
            }
        }
    }
    
    func read(delegate: RootPresenterDelegate?) {
        SECodable.instance.read(filename: userFilename) { (user: UserModel?, error: NSError?) in
            if let error = error {
                delegate?.onError(message: error.localizedDescription)
            } else {
                self.user = user
                delegate?.onReadUser()
            }
        }
    }
    
    func logout(delegate: RootPresenterDelegate?) {
        DLDefault.userToken.remove()
        SECodable.instance.remove(filename: userFilename) { error in
            if let error = error {
                delegate?.onError(message: error.localizedDescription)
            } else {
                delegate?.onLogoutUser()
            }
        }
    }
    
    func getUserMe(delegate: UserPresenterDelegate?) {
        let request = GetUserMeRequest()
        request.performRequest(delegate: delegate) {
            request.performResponse { response in
                if response.status == false {
                     delegate?.onError(message: response.message)
                 } else if let user = response.data {
                    self.write(object: user) { error in
                        if let error = error {
                            delegate?.onError(message: error.localizedDescription)
                        } else {
                            delegate?.onGetUserMe()
                        }
                    }
                 } else {
                     delegate?.onError()
                 }
            }
        }
    }
    
    func putUserMe(delegate: UserPresenterDelegate?, data: UserModel) {
        let request = PutUserMeRequest(data: data)
        request.performRequest(delegate: delegate) {
            request.performResponse { response in
                if response.status == false {
                    delegate?.onError(message: response.message)
                } else if let user = response.data?.user {
                    self.write(object: user) { error in
                        if let error = error {
                            delegate?.onError(message: error.localizedDescription)
                        } else {
                            delegate?.onPutUserMe()
                        }
                    }
                } else {
                    delegate?.onError()
                }
            }
        }
    }
    
    func getUserNotification(delegate: UserPresenterDelegate?) {
        let request = GetUserNotificationRequest()
        request.performRequest(delegate: delegate) {
            request.performResponse { response in
                if response.status == false {
                    delegate?.onError(message: response.message)
                } else if let notifications = response.data?.notifications {
                    delegate?.onGetUser(notification: notifications)
                } else {
                    delegate?.onError()
                }
            }
        }
    }
    
    func postUserQuestionnaire(delegate: UserPresenterDelegate?) {
        let request = PostUserQuestionnaireRequest()
        request.performRequest(delegate: delegate) {
            request.performResponse { response in
                if response.status == false {
                    delegate?.onError(message: response.message)
                } else if let _ = response.data {
                    delegate?.onPostUserQuestionnaire()
                } else {
                    delegate?.onError()
                }
            }
        }
    }
    
    func postUserContact(delegate: UserPresenterDelegate?, data: ContactModel) {
        let request = PostUserContactRequest(data: data)
        request.performRequest(delegate: delegate) {
            request.performResponse { response in
                if response.status == false {
                    delegate?.onError(message: response.message)
                } else if let _ = response.data {
                    delegate?.onPostUserContact()
                } else {
                    delegate?.onError()
                }
            }
        }
    }
}

extension UserManager {
    
    private var positionMinTime: TimeInterval {
        return 15.0 * 60.0 // 15 minutes
    }
    
    private var positionMinDistance: CLLocationDistance {
        if let value = SettingManager.instance.setting(code: .safehouseRangeArea)?.value {
            if let double = NumberFormatter().number(from: value)?.doubleValue {
                return double
            }
        }
        return 50.0 // 50 meters
    }
    
    private func on(location: CLLocation?) {
        guard let location = location else { return }
        
        if let safehouse = user?.safehouse?.coordinates?.location {
            if safehouse.distance(from: safehouse) > positionMinDistance {
                postGeoNewPoint(location: location)
                return
            }
        }

        if let last = positionTime {
            if last.addingTimeInterval(positionMinTime).timeIntervalSince1970 < Date().timeIntervalSince1970 {
                postGeoNewPoint(location: location)
            }
        } else {
            postGeoNewPoint(location: location)
        }
    }
    
    private func postGeoNewPoint(location: CLLocation) {
        positionTime = Date()
        let newPoint = NewPointModel()
        newPoint.point = PointModel(latitude: location.coordinate.latitude,
                                    longitude: location.coordinate.longitude)
        GeoManager.instance.postGeoNewPoint(delegate: nil, data: newPoint)
    }
}
