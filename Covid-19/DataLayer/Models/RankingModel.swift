//
//  RankingModel.swift
//  Covid-19
//
//  Created by Juan Pablo on 03/04/2020.
//  Copyright © 2020 eCloudSolutions. All rights reserved.
//

import Foundation

class RankingModel: NSObject, Codable {
    
    var areInfected: Int?
    var fellows: Int?
    var id: Int?
    var name: String?
}
