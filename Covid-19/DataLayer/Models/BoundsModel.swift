//
//  BoundsModel.swift
//  Covid-19
//
//  Created by Juan Pablo on 04/04/2020.
//  Copyright © 2020 eCloudSolutions. All rights reserved.
//

import Foundation

class BoundsModel: NSObject, Codable {
    
    var id: Int?
    var topLeft: PointModel?
    var topRight: PointModel?
    var bottomLeft: PointModel?
    var bottomRight: PointModel?
}
