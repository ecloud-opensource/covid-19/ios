//
//  CityRankingModel.swift
//  Covid-19
//
//  Created by Gonzalo Sanchez on 18/04/2020.
//  Copyright © 2020 eCloudSolutions. All rights reserved.
//

import Foundation
import UIKit

class CityRankingModel: NSObject, Codable {
    var cityID: String?
    var countryID: String?
    init(cityID: String? = nil, countryID: String? = nil) {
        self.cityID = cityID
        self.countryID = countryID
    }
}
