//
//  CountryModel.swift
//  Covid-19
//
//  Created by Juan Pablo on 30/03/2020.
//  Copyright © 2020 eCloudSolutions. All rights reserved.
//

import Foundation

class CountryModel: NSObject, Codable {
    
    var id: Int?
    var name: String?
    var code: String?
    var termsAndConditions: String?
    var center: PointModel?
    var area: PolygonModel?
    var cities: [CityModel]?
    var link: [CityFeedModel]?
}
