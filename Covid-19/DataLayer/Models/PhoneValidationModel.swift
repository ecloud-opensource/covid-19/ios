//
//  PhoneValidationModel.swift
//  Covid-19
//
//  Created by Juan Pablo on 30/03/2020.
//  Copyright © 2020 eCloudSolutions. All rights reserved.
//

import Foundation
import UIKit

class PhoneValidationModel: NSObject, Codable {
    
    var id: Int?
    var phone: String?
    var identification: String?
    var code: String?
    var validated: Bool?
    var whatsappinstalled: Bool {
        #if targetEnvironment(simulator)
        return true
        #else
        if let url = URL(string: "whatsapp://") {
            return UIApplication.shared.canOpenURL(url)
        } else {
            return false
        }
        #endif
    }
}
