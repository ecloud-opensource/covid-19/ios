//
//  UserModel.swift
//  Covid-19
//
//  Created by Juan Pablo on 28/03/2020.
//  Copyright © 2020 eCloudSolutions. All rights reserved.
//

import Foundation
import SwiftExtension

enum UserStatus: String {
    case quarentined = "QUARANTINED"
    case onRisk = "ON_RISK"
    case exposed = "EXPOSED"
    case withSymptoms = "WITH_SYMPTOMS"
    case healthy = "HEALTHY"
    var title: String {
        switch self {
        case .quarentined:
            return R.localizable.statusWithSymptoms()
        case .onRisk:
            return R.localizable.statusRiskGroup()
        case .exposed:
            return R.localizable.statusContact()
        case .withSymptoms:
            return R.localizable.statusWithSymptoms()
        case .healthy:
            return R.localizable.statusNoSymptoms()
        }
    }
    var image: UIImage? {
        switch self {
        case .quarentined:
            return R.image.iconStatusQuarantined()
        case .onRisk:
            return R.image.iconStatusRiskgroup()
        case .exposed:
            return R.image.iconStatusContact()
        case .withSymptoms:
            return R.image.iconStatusSymptoms()
        case .healthy:
            return R.image.iconStatusNosymptoms()
        }
    }
}

class UserModel: NSObject, Codable {

    var id: Int?
    var firstName: String?
    var lastName: String?
    var phone: String?
    var email: String?
    var address: String?
    var number: String?
    var safehouse: PointModel?
    var isQuarantined: Bool?
    var finishQuarantine: Bool?
    var quarantineFrom: String?
    var quarantineTo: String?
    var identification: String?
    var gender: String? // Allowed values: "M", "F", "SD"
    var pregnant: Bool?
    var probability: Float?
    var outOfSafehouse: Bool?
    var onMovement: Bool?
    var status: String?
    var country: CountryModel?
    var latestPosition: PositionModel?
    var neighborhood: NeighborhoodModel?
    var city: CityModel?
    var zone: ZoneModel?
    
    var fullName: String {
        return "\(firstName ?? "") \(lastName ?? "")"
    }
    
    var fullAddress: String {
        return "\(address ?? "") \(number ?? "")"
    }
    
    var statusValue: UserStatus? {
        return UserStatus(optValue: status)
    }
}
