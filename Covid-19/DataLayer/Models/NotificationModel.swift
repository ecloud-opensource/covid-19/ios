//
//  NotificationModel.swift
//  Covid-19
//
//  Created by Juan Pablo on 01/04/2020.
//  Copyright © 2020 eCloudSolutions. All rights reserved.
//

import Foundation

class NotificationModel: NSObject, Codable {
    
    var id: Int?
    var type: String?
    var title: String?
    
    var isHeader: Bool {
        return type?.uppercased() == "HEADER"
    }
    
    var isFooter: Bool {
        return type?.uppercased() == "FOOTER"
    }
    
}
