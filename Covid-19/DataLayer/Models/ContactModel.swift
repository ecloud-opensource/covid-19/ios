//
//  ContactModel.swift
//  Covid-19
//
//  Created by Juan Pablo on 04/04/2020.
//  Copyright © 2020 eCloudSolutions. All rights reserved.
//

import Foundation

class ContactModel: NSObject, Codable {
    
    var id: Int?
    var firstName: String?
    var lastName: String?
    var email: String?
    var message: String?
    
    var validated: Bool {
        guard firstName?.isNotEmpty ?? false, lastName?.isNotEmpty ?? false, email?.isValidEmail() ?? false, message?.isNotEmpty ?? false else {
            return false
        }
        return true
    }
}
