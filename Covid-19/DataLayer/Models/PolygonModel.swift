//
//  PolygonModel.swift
//  Covid-19
//
//  Created by Juan Pablo on 30/03/2020.
//  Copyright © 2020 eCloudSolutions. All rights reserved.
//

import Foundation

class PolygonModel: NSObject, Codable {
    
    var id: Int?
    var type: String? // Allowed values: "Polygon"
    var coordinates: [[Coordinates]]?
}
