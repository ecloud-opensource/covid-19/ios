//
//  PositionModel.swift
//  Covid-19
//
//  Created by Juan Pablo on 30/03/2020.
//  Copyright © 2020 eCloudSolutions. All rights reserved.
//

import Foundation

class PositionModel: NSObject, Codable {
    
    var id: Int?
    var position: PointModel?
    var nearQuarantine: Bool?
    var user: UserModel?
}
