//
//  NeighborhoodModel.swift
//  Covid-19
//
//  Created by Juan Pablo on 30/03/2020.
//  Copyright © 2020 eCloudSolutions. All rights reserved.
//

import Foundation

class NeighborhoodModel: NSObject, Codable {
 
    var id: Int?
    var name: String?
    var code: String?
    var termsAndConditions: String?
    var areInfected: Int?
    var wereInfected: Int?
    var fellows: Int?
    var center: PointModel?
    var area: PolygonModel?
    var zones: [ZoneModel]?
    var city: CityModel?
    var country: CountryModel?
}
