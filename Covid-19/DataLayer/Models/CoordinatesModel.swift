//
//  CoordinatesModel.swift
//  Covid-19
//
//  Created by Juan Pablo on 08/04/2020.
//  Copyright © 2020 eCloudSolutions. All rights reserved.
//

import Foundation
import CoreLocation

typealias Coordinates = [Double]

extension Coordinates {
    
    init(latitude: Double, longitude: Double) {
        self.init()
        insert(latitude, at: 0)
        insert(longitude, at: 1)
    }
    
    var latitude: CLLocationDegrees? {
        guard count > 0 else { return nil }
        return self[0]
    }
    
    var longitude: CLLocationDegrees? {
        guard count > 1 else { return nil }
        return self[1]
    }
    
    var location: CLLocation? {
        guard let latitude = latitude, let longitude = longitude else { return nil }
        return CLLocation(latitude: latitude, longitude: longitude)
    }
}
