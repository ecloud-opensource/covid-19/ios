//
//  CityFeedModel.swift
//  Covid-19
//
//  Created by Juan Pablo on 03/04/2020.
//  Copyright © 2020 eCloudSolutions. All rights reserved.
//

import Foundation

class CityFeedModel: NSObject, Codable {
    
    var id: Int?
    var idCity: Int?
    var order: Int?
    var url: String?
    var title: String?
    var image: String?
    var info: String?
    var city: CityModel?
    
    enum CodingKeys: String, CodingKey {
        case id
        case url
        case title
        case image
        case info = "description"
        case city
    }
}
