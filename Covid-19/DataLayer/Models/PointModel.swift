//
//  PointModel.swift
//  Covid-19
//
//  Created by Juan Pablo on 30/03/2020.
//  Copyright © 2020 eCloudSolutions. All rights reserved.
//

import Foundation

class PointModel: NSObject, Codable {

    var id: Int?
    var type: String? // Allowed values: "Point"
    var coordinates: Coordinates?
    
    init(latitude: Double, longitude: Double) {
        id = nil
        type = "Point"
        coordinates = Coordinates(latitude: latitude, longitude: longitude)
    }
}
