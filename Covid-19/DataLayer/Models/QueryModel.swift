//
//  QueryModel.swift
//  Covid-19
//
//  Created by Juan Pablo on 08/04/2020.
//  Copyright © 2020 eCloudSolutions. All rights reserved.
//

import Foundation

class QueryModel: NSObject, Codable {
    
    var page: Int?
    var pageSize: Int?
    var from: String?
    var to: String?
    var term: String?
    var scope: String?
    var group: String?
    var order: String?
    var status: String?
    
    init(scope: [String]? = nil) {
        self.scope = scope?.joined(separator: ",")
    }
}
