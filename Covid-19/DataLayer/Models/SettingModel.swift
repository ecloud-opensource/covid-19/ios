//
//  SettingModel.swift
//  Covid-19
//
//  Created by Juan Pablo on 07/04/2020.
//  Copyright © 2020 eCloudSolutions. All rights reserved.
//

import Foundation

enum SettingCode: String {
    case safehouseRangeArea = "SAFEHOUSE_RANGE_AREA"
    case SMSResendTime = "SMS_RESEND_TIME"
    case phonePinExpiration = "PHONE_PIN_EXPIRATION"
}

class SettingModel: NSObject, Codable {
    
    var id: Int?
    var name: String?
    var code: String?
    var info: String?
    var value: String?
    
    enum CodingKeys: String, CodingKey {
        case id
        case name
        case code
        case info = "description"
        case value
    }
    
    var codeValue: SettingCode? {
        return SettingCode(optValue: code)
    }
}
