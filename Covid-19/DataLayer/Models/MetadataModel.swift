//
//  MetadataModel.swift
//  Covid-19
//
//  Created by Juan Pablo on 03/04/2020.
//  Copyright © 2020 eCloudSolutions. All rights reserved.
//

import Foundation

class MetadataModel: NSObject, Codable {
    
    var currentPage: Int?
    var pageSize: Int?
    var totalResults: Int?
}
