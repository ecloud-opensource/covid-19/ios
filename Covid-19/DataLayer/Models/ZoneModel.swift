//
//  ZoneModel.swift
//  Covid-19
//
//  Created by Juan Pablo on 30/03/2020.
//  Copyright © 2020 eCloudSolutions. All rights reserved.
//

import Foundation

class ZoneModel: NSObject, Codable {
    
    var id: Int?
    var name: String?
    var center: PointModel?
    var area: PolygonModel?
    var neighborhood: NeighborhoodModel?
}
