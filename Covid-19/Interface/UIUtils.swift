//
//  UIUtils.swift
//  Covid-19
//
//  Created by Gonzalo Sanchez on 31/03/2020.
//  Copyright © 2020 eCloudSolutions. All rights reserved.
//

import Foundation
import SwiftExtension
import SafariServices

func debugLog(_ string: String) {
    #if DEBUG
    print(string)
    #endif
}

class COBEATUIUtils {
    
    static func openInternalBrowser(withStringURL url: String) {
        guard let url = URL(string: url) else {
            return
        }
        let svc = SFSafariViewController(url: url)
        DispatchQueue.main.async {
            if let vc = UIApplication.shared.keyWindow?.rootViewController?.presentedViewController {
                vc.present(svc, animated: true, completion: nil)
            } else {
                UIApplication.shared.keyWindow?.rootViewController?.present(svc, animated: true, completion: nil)
            }
        }
    }
    
    static func share(items: [Any], fromVC originController: UIViewController) {
        let controller = UIActivityViewController(activityItems: items, applicationActivities: [])
        originController.present(controller, animated: true)
    }
    
    static func termsURL() -> String {
        return (try? UIDefault.termsURL.get()) ?? HardCoded.landingURL.value
    }
    
    static func quizURL() -> String {
        if let token: String = try? DLDefault.userToken.get() {
            let language = DLLanguage.current().rawValue
            return "https://www.cobeat.app/questionnaire.html?x-api-key=\(token)&accept-language=\(language)"
        }
        return ""
    }
    
}
