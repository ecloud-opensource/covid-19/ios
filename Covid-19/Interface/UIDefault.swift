//
//  UIDefault.swift
//  Covid-19
//
//  Created by Gonzalo Sanchez on 03/04/2020.
//  Copyright © 2020 eCloudSolutions. All rights reserved.
//

import Foundation
import SwiftExtension

enum UIDefault: String, SEDefaultDelegate {
    case SMSTimer = "sms_timer"
    case SMSTimerLastNumber = "sms_timer_last_number"
    case termsURL = "terms_url"
}
