//
//  UIConstants.swift
//  Covid-19
//
//  Created by Juan Pablo on 30/03/2020.
//  Copyright © 2020 eCloudSolutions. All rights reserved.
//

import UIKit

enum FirebaseMessagingTopic: String {
    case test
    case general
    case ios
}

enum ReusableControllerType {
    case drawer
    case helper
    case undismisssable
}

enum HardCoded {
    case landingURL
    case emergencyNumber
    case reportNumber
    var value: String {
        switch self {
        case .landingURL:
            return "https://www.cobeat.app"
        case .emergencyNumber:
            return "107"
        case .reportNumber:
            return "134"
        }
    }
}

public enum GoogleMaps {
    case apiKey
    var value: String {
        switch self {
        case .apiKey : return AppKeys.googleMaps
        }
    }
}

public enum UINotificationName {
    case unauthorized
    case FCMToken
    case applicationDidBecomeActive
    public var value: Notification.Name {
        switch self {
        case .unauthorized:                 return Notification.Name("unauthorized")
        case .FCMToken:                     return Notification.Name("FCMToken")
        case .applicationDidBecomeActive:   return Notification.Name("applicationDidBecomeActive")
        }
    }
}


enum COBEATMarkerViewType {
    case user
    case collaborator
    case infected
    var icon: UIImage? {
        switch self {
        case .user:
            return R.image.iconUserLocation()
        case .collaborator:
            return R.image.iconCollaborator()
        case .infected:
            return R.image.iconInfected()
        }
    }
}

enum GMSPlacesComponent {
    case address
    case addressNumber
    var value: String {
        switch self {
        case .address:
            return "route"
        case .addressNumber:
            return "street_number"
        }
    }
}

enum DrawerSectionType {
    case header
    case profile
    case links
    case ranking
    case collaborate
    case terms
    case footer
    var title: String {
        switch self {
        case .header:
            return ""
        case .profile:
            return R.localizable.profile()
        case .links:
            return R.localizable.links()
        case .ranking:
            return R.localizable.cityRanking()
        case .collaborate:
            return R.localizable.collaborate()
        case .terms:
            return R.localizable.terms()
        case .footer:
            return ""
        }
    }
}

enum HomeUtilsCellType {
    case flap
    case info
    case telephones
    case news
    case empty
    var contentBackgroundColor: UIColor {
        switch self {
        case .info:
            return .cobeatGrayBackground
        case .telephones:
            return .white
        case .news:
            return .cobeatGrayBackground
        default:
            return .clear
        }
    }
    var mainBackgroundColor: UIColor {
        switch self {
        case .info:
            return .white
        case .telephones:
            return .cobeatGrayBackground
        case .news:
            return .cobeatNewsCellBackground
        default:
            return .clear
        }
    }
    var topBackgroundColor: UIColor {
        switch self {
        case .info:
            return .cobeatPrimaryPrimary
        case .telephones:
            return .cobeatRedBackgronud
        case .news:
            return .clear
        default:
            return .clear
        }
    }
    var topTitle: String {
        switch self {
        case .info:
            return R.localizable.checkInfectionProbability()
        case .telephones:
            return R.localizable.presentsSyntomps()
        case .news:
            return ""
        default:
            return ""
        }
    }
    var topTitleFont: UIFont {
        switch self {
        case .info:
            return .gothamRoundedBook(15)
        case .telephones:
            return .gothamRoundedBook(12)
        case .news:
            return .gothamRoundedBook(15)
        default:
            return .gothamRoundedBook(15)
        }
    }
    var topTitleColor: UIColor {
        switch self {
        case .info:
            return .white
        case .telephones:
            return .white
        case .news:
            return .clear
        default:
            return .clear
        }
    }
    var topSubtitle: String {
        switch self {
        case .info:
            return R.localizable.checkNow().uppercased()
        case .telephones:
            return R.localizable.callSympton()
        case .news:
            return ""
        default:
            return ""
        }
    }
    var topSubtitleFont: UIFont {
        switch self {
        case .info:
            return .gothamRoundedMedium(12)
        case .telephones:
            return .gothamRoundedMedium(17)
        case .news:
            return .gothamRoundedMedium(12)
        default:
            return .gothamRoundedMedium(12)
        }
    }
    var topSubtitleColor: UIColor {
        switch self {
        case .info:
            return UIColor.white.withAlphaComponent(0.5)
        case .telephones:
            return .white
        case .news:
            return .clear
        default:
            return .clear
        }
    }
    var bottomBackgroundColor: UIColor {
        switch self {
        case .info:
            return .cobeatLightBrownBackground
        case .telephones:
            return .cobeatLightBrownBackground
        case .news:
            return .clear
        default:
            return .clear
        }
    }
    var bottomTitle: String {
        switch self {
        case .info:
            return R.localizable.togetherStopVirus()
        case .telephones:
            return R.localizable.reportByCall()
        case .news:
            return ""
        default:
            return ""
        }
    }
    var bottomTitleFont: UIFont {
        switch self {
        case .info:
            return .gothamRoundedBook(15)
        case .telephones:
            return .gothamRoundedMedium(17)
        case .news:
            return .gothamRoundedMedium(12)
        default:
            return .gothamRoundedMedium(12)
        }
    }
    var bottomTitleColor: UIColor {
        switch self {
        case .info:
            return .white
        case .telephones:
            return .white
        default:
            return .clear
        }
    }
    var bottomSubtitle: String {
        switch self {
        case .info:
            return R.localizable.shareApp().uppercased()
        case .telephones:
            return R.localizable.callReport()
        case .news:
            return ""
        default:
            return ""
        }
    }
    var bottomSubtitleFont: UIFont {
        switch self {
        case .info:
            return .gothamRoundedMedium(12)
        case .telephones:
            return .gothamRoundedBook(13)
        case .news:
            return .gothamRoundedMedium(12)
        default:
            return .gothamRoundedMedium(12)
        }
    }
    var bottomSubtitleColor: UIColor {
        switch self {
        case .info:
            return UIColor.white.withAlphaComponent(0.5)
        case .telephones:
            return .white
        default:
            return .clear
        }
    }
    
}

enum ProfileCellType {
    case empty
    case footer
    case firstname
    case lastname
    case email
    case address
    case phone
    case identification
    case status
    var title: String {
        switch self {
        case .empty, .footer:
            return ""
        case .firstname:
            return R.localizable.firstnameTitle()
        case .lastname:
            return R.localizable.lastnameTitle()
        case .email:
            return R.localizable.emailTitle()
        case .address:
            return R.localizable.addressTitle()
        case .phone:
            return R.localizable.phoneTitle()
        case .identification:
            return R.localizable.idTitle()
        case .status:
            return ""
        }
    }
    var placeholder: String {
        switch self {
        case .empty, .footer:
            return ""
        case .firstname:
            return R.localizable.firstnamePlaceholder()
        case .lastname:
            return R.localizable.lastnamePlaceholder()
        case .email:
            return R.localizable.emailPlaceholder()
        case .address:
            return R.localizable.addressPlaceholder()
        case .phone:
            return R.localizable.phonePlaceholder()
        case .identification:
            return R.localizable.idTitlePlaceholder()
        case .status:
            return ""
        }
    }
}

enum TextfieldType {
    case telephone
    case verificationPIN
    case firstName
    case lastName
    case email
    case address
    case identification
}
