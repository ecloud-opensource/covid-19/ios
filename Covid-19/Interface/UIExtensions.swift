//
//  Extensions.swift
//  Covid-19
//
//  Created by Gonzalo Sanchez on 30/03/2020.
//  Copyright © 2020 eCloudSolutions. All rights reserved.
//

import Foundation
import SwiftExtension
import KYDrawerController
import Firebase
import GoogleMaps
import GooglePlaces

// MARK: - Classes

class COBEATBaseViewController: BaseViewController {
    
    var isVisibleViewController: Bool {
        return self.isViewLoaded && (self.view.window != nil)
    }
    
    func setCOBEATNavigationBar(withTitle title: String) {
        navigationController?.navigationBar.tintColor = UIColor.white
        navigationController?.navigationBar.barTintColor = .white
        navigationItem.setCenterTitle(title, color: .cobeatPrimaryPrimary)
    }
    
    func setCOBEATNavigationBarWithLogo() {
        navigationController?.navigationBar.tintColor = UIColor.white
        navigationController?.navigationBar.barTintColor = .white
        let view = UIView(frame: CGRect(origin: CGPoint.zero, size: CGSize(width: 100, height: 35)))
        let logo = UIImageView(image: R.image.cobeatWideBlue() ?? UIImage())
        logo.frame = view.bounds
        logo.contentMode = .scaleAspectFit
        view.addSubview(logo)
        navigationItem.titleView = view
    }
    
    func setNavigationBarHidden(_ hidden: Bool = true, animated: Bool = true) {
        navigationController?.setNavigationBarHidden(hidden, animated: animated)
    }
    
    func hideBackButton() {
        navigationItem.setHidesBackButton(true, animated:true);
    }
    
    func showSnackBar(withSuccess message: String, duration: TimeInterval = BannerView.viewDefaultTime, height: CGFloat = BannerView.viewHeight) {
        BannerView.show(message, mode: .success, time: duration, height: height)
    }
    
    func showSnackBar(withAlert message: String, duration: TimeInterval = BannerView.viewDefaultTime, height: CGFloat = BannerView.viewHeight) {
        BannerView.show(message, mode: .alert, time: duration, height: height)
    }
    
    func showSnackBar(withError message: String, duration: TimeInterval = BannerView.viewDefaultTime, height: CGFloat = BannerView.viewHeight) {
        BannerView.show(message, mode: .error, time: duration, height: height)
    }
    
    @objc func onDrawerLeftMenuButton() {
        if let drawerController = navigationController?.parent as? KYDrawerController {
            drawerController.setDrawerState(.opened, animated: true)
        }
    }
    
    @objc func onDrawerRightMenuButton() {
        if let drawerController = navigationController?.parent as? KYDrawerController {
            drawerController.setDrawerState(.opened, animated: true)
        }
    }
    
    func setOpenDrawerGestureModeMaskNone() {
        if let drawerController = navigationController?.parent as? KYDrawerController {
            drawerController.screenEdgePanGestureEnabled = false
        }
    }
    
    func setOpenDrawerGestureModeMaskAll() {
        if let drawerController = navigationController?.parent as? KYDrawerController {
            drawerController.screenEdgePanGestureEnabled = true
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setOpenDrawerGestureModeMaskNone()
        interactivePopGestureRecognizer(false)
    }
    
    override func keyboardWillShow(_ keyboardHeight: CGFloat) {
        self.view.frame.origin.y = 0
        self.view.frame.origin.y -= keyboardHeight/2
        self.view.layoutIfNeeded()
    }
    
    override func keyboardWillHide(_ keyboardHeight: CGFloat) {
        self.view.frame.origin.y = 0
        self.view.layoutIfNeeded()
    }
    
}

extension BannerView {
    class func configure() {
        viewFont = .gothamRoundedBook(14)
        viewSuccess = R.image.iconSuccess.name
        viewAlert = R.image.iconWarning.name
        viewError = R.image.iconError.name
        colorSuccess = .cobeatStatusSuccess
        colorAlert = .cobeatStatusWarning
        colorError = .cobeatStatusError
        viewHeight = 100
    }
}

class TextfieldWithError: UITextField {
    
    private var label: UILabel?
    private var underlineView: UIView?
    private var defaultUnderlineColor: UIColor?

    func underlinedForError(color: UIColor = .lightGray, lineHeight: CGFloat = 1) {
        defaultUnderlineColor = color
        underlineView = UIView(frame: CGRect(x: 0, y: frame.height + lineHeight, width: frame.width, height: lineHeight))
        underlineView?.backgroundColor = color
        underlineView?.autoresizingMask = [UIView.AutoresizingMask.flexibleWidth, UIView.AutoresizingMask.flexibleTopMargin]
        addSubview(underlineView ?? UIView())
        layer.masksToBounds = false
    }
    
    func showErrorLabel(_ message: String, textColor: UIColor = .cobeatRed, topSpace: CGFloat = 5) {
        label?.removeFromSuperview()
        label = UILabel()
        label?.text = message
        label?.set(font: UIFont.gothamRoundedBook(12), andColor: textColor)
        label?.textAlignment = .left
        label?.numberOfLines = 0
        label?.alpha = 0
        addSubview(label!)
        addConstraints(topSpace)
        UIView.animate(withDuration: Double(0.4)) {
            self.label?.alpha = 1
            if let underline = self.underlineView {
                underline.backgroundColor = textColor
            }
        }
    }
    
    func hideErrorLabel() {
        UIView.animate(withDuration: Double(0.4), animations: {
            self.label?.alpha = 0
            if let underline = self.underlineView {
                underline.backgroundColor = self.defaultUnderlineColor
            }
        }) { (bool) in
            self.label?.removeFromSuperview()
        }
    }
    
    private func addConstraints(_  topSpace: CGFloat) {
        label?.translatesAutoresizingMaskIntoConstraints = false
        
        let attributeLeading = NSLayoutConstraint.Attribute.leading
        let attributeTrailing = NSLayoutConstraint.Attribute.trailing
        let attributeTop = NSLayoutConstraint.Attribute.top
        let attributeBottom = NSLayoutConstraint.Attribute.bottom

        let relationEqual = NSLayoutConstraint.Relation.equal
        
        let leadingConstraint = NSLayoutConstraint(item: label!, attribute: attributeLeading, relatedBy: relationEqual, toItem: self, attribute: attributeLeading, multiplier: 1, constant: 0)
        let trailingConstraint = NSLayoutConstraint(item: label!, attribute: attributeTrailing, relatedBy: relationEqual, toItem: self, attribute: attributeTrailing, multiplier: 1, constant: 0)
        let topConstraint = NSLayoutConstraint(item: label!, attribute: attributeTop, relatedBy: relationEqual, toItem: self, attribute: attributeBottom, multiplier: 1, constant: topSpace)
        addConstraints([leadingConstraint, trailingConstraint, topConstraint])
    }
    
}

class COBEATMarkerView: GMSMarker {
    
    static func create(withIcon icon: UIImageView, iconSize: CGFloat, backgroundColor: UIColor, circle: Bool = true) -> UIView {
        
        let mainView = UIView(frame: CGRect(x: 0, y: 0, width: iconSize + 10, height: iconSize + 10))
        
        let content = UIView(frame: CGRect(x: 0, y: 0, width: iconSize, height: iconSize))
        content.backgroundColor = backgroundColor
        content.center = mainView.center
        if circle {
            content.circle()
        }
        content.clipsToBounds = true
        content.addShadow(color: .black, radious: 3)
        
        icon.frame = CGRect(x: 0, y: 0, width: iconSize, height: iconSize)
        if let subview = icon.viewWithTag(100) {
            subview.frame = icon.frame
        }
        icon.contentMode = .scaleAspectFit
        icon.center = mainView.center
        
        mainView.addSubview(content)
        mainView.addSubview(icon)
        return mainView
    }
    
}

protocol EresableTextFieldDelegate {
    func textfieldDidEnterBackward(_ textield: UITextField)
}

class EresableTextField: UITextField {
    
    var eresableTextFieldDelegate: EresableTextFieldDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func deleteBackward() {
        super.deleteBackward()
        if text?.isEmpty ?? true {
            eresableTextFieldDelegate?.textfieldDidEnterBackward(self)
        }
    }
    
}

// MARK: - Extensions

extension UINavigationItem {
    func setLeftMenuButton(viewController vc: COBEATBaseViewController, color: UIColor? = .cobeatPrimaryPrimary) {
        leftBarButtonItem = UIBarButtonItem(
            image: UIImage(named:navigationItemIconLeft),
            style: UIBarButtonItem.Style.plain,
            target: vc,
            action: #selector(vc.onDrawerLeftMenuButton))
        leftBarButtonItem?.tintColor = color
    }
    
    func setCloseLeftButton(viewController vc: COBEATBaseViewController, color: UIColor? = .white) {
        leftBarButtonItem = UIBarButtonItem(
            image: UIImage(named:navigationItemIconClose),
            style: UIBarButtonItem.Style.plain,
            target: vc,
            action: #selector(vc.closeRightButton))
        leftBarButtonItem?.tintColor = color
    }
}

extension R {
    static let localizable = string.localizable.self
}

extension UIColor {
    class var cobeatPrimaryPrimary: UIColor {
        return UIColor(hexString: "#0073BC") ?? .black
    }
    
    class var cobeatPrimarySecondary: UIColor {
        return UIColor(hexString: "#006EB3") ?? .black
    }
    
    class var cobeatOrage: UIColor {
        return UIColor(hexString: "#FF7F33") ?? .black
    }
    
    class var cobeatRed: UIColor {
        return UIColor(hexString: "#FC4343") ?? .black
    }
    
    class var cobeatStatusSuccess: UIColor {
        return UIColor(hexString: "#00BC61") ?? .black
    }
    
    class var cobeatStatusWarning: UIColor {
        return UIColor(hexString: "#FF7F33") ?? .black
    }
    
    class var cobeatStatusError: UIColor {
        return UIColor(hexString: "#E04D4D") ?? .black
    }
    
    class var cobeatGrayBackground: UIColor {
        return UIColor(hexString: "#ECE9E5") ?? .black
    }
    
    class var cobeatNewsCellBackground: UIColor {
        return UIColor(hexString: "#D9D9D9") ?? .black
    }
    
    class var cobeatLightBrownBackground: UIColor {
        return UIColor(hexString: "#C2B6A5") ?? .black
    }
    
    class var cobeatRedBackgronud: UIColor {
        return UIColor(hexString: "#AB4242") ?? .black
    }
    
}

extension String {
    var isNotEmpty: Bool {
        return !self.isEmpty
    }
}

extension UIFont {
    class func gothamRoundedMedium(_ size: Int) -> UIFont {
        return UIFont(name: "GothamRounded-Medium", size: CGFloat(size)) ?? UIFont.systemFont(ofSize: CGFloat(size))
    }
    class func gothamRoundedBook(_ size: Int) -> UIFont {
        return UIFont(name: "GothamRounded-Book", size: CGFloat(size)) ?? UIFont.systemFont(ofSize: CGFloat(size))
    }
    class func gothamRoundedLight(_ size: Int) -> UIFont {
        return UIFont(name: "GothamRounded-Light", size: CGFloat(size)) ?? UIFont.systemFont(ofSize: CGFloat(size))
    }
    class func SFProSemibold(_ size: Int) -> UIFont {
        return UIFont(name: "SFProText-Semibold", size: CGFloat(size)) ?? UIFont.systemFont(ofSize: CGFloat(size))
    }
}

extension UITextField {
    
    func roundedField(_ value: CGFloat, withColor color: UIColor) {
        self.borderStyle = .none
        self.layer.cornerRadius = value
        self.layer.backgroundColor = color.cgColor
    }
    
    func setPadding(_ amount: CGFloat) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.leftView = paddingView
        self.rightView = paddingView
        self.leftViewMode = .always
        self.rightViewMode = .always
    }
}

extension GMSMapView {
    
    func addAnnotation(withType type: COBEATMarkerViewType, location: CLLocationCoordinate2D, andData item: Any? = nil) {
        switch type {
        case .user:
            addAnnotation(location, icon: UIImageView(image: type.icon), iconSize: 35)
        case .collaborator:
            addAnnotation(location, icon: UIImageView(image: type.icon))
        case .infected:
            addAnnotation(location, icon: UIImageView(image: type.icon))
        }
    }
    
    private func addAnnotation(_ location: CLLocationCoordinate2D, data: Any? = nil, icon: UIImageView? = nil, iconSize: CGFloat = 25, appearAnimation: GMSMarkerAnimation = .none, backgroundColor: UIColor = .clear) {
        let marker = GMSMarker(position: location)
        if let icon = icon {
            marker.iconView = COBEATMarkerView.create(withIcon: icon, iconSize: iconSize, backgroundColor: backgroundColor)
            marker.appearAnimation = appearAnimation
        }
        marker.userData = data
        marker.map = self
    }
    
}

extension Int {
    
    var double: Double {
        return Double(self)
    }
    
    var formattedToMinutesAndSeconds: String {
        let minutes = (self % 3600)/60
        let seconds = (self % 3600)%60
        let minutesString = minutes < 10 ? "0\(minutes.description)" : minutes.description
        let secondString = seconds < 10 ? "0\(seconds.description)" : seconds.description
        return "\(minutesString):\(secondString)"
    }
    
    var formattedToMinutesAndSecondsFull: String {
        let minutes = (self % 3600)/60
        let seconds = (self % 3600)%60
        let minutesString = minutes >= 1 ? "\(minutes.description) \(minutes > 1 ? "minutos" : "minuto" )" : ""
        let secondString = "\(seconds.description) \(seconds > 1 ? "segundos" : "segundo")"
        return "\(!minutesString.isEmpty ? "\(minutesString) y " : "")\(secondString)"
    }
    
}

extension AppDelegate {
    
    func configure(application: UIApplication, launchOptions: [UIApplication.LaunchOptionsKey: Any]?) {
        BannerView.configure()
        configureGoogleMaps()
        configureGooglePlaces()
        configureFirebase()
        setSwiftExtensionsVariables()
        UIApplication.shared.statusBarStyle = .lightContent
        self.application(application, launchOptions: launchOptions)
    }
    
    func setSwiftExtensionsVariables() {
        navigationItemIconLeft = R.image.iconDrawer.name
        navigationItemIconClose = R.image.iconClose.name
        navigationItemIconBack = R.image.iconBack.name
//        navigationItemIconDone = R.image.iconDone.name
        navigationItemTitleFont = UIFont.gothamRoundedBook(18)
        DefaultTimeZoneAbbreviation = "UTC"
        DefaultLocaleIdentifier = "es_AR"
    }
    
    func configureGoogleMaps() {
        GMSServices.provideAPIKey(GoogleMaps.apiKey.value)
    }
    
    func configureGooglePlaces() {
        GMSPlacesClient.provideAPIKey(GoogleMaps.apiKey.value)
    }
    
    func configureFirebase() {
        FirebaseApp.configure()
//        Config.setDefaults()
    }
    
    func application(_ application: UIApplication, launchOptions: [AnyHashable: Any]?) {
        if let options = launchOptions {
            if let remoteNotification = options[UIApplication.LaunchOptionsKey.remoteNotification] as? [AnyHashable: Any] {
                SEMessaging.instance.did(receiveRemoteNotification: remoteNotification)
            }
            if let _ = options[UIApplication.LaunchOptionsKey.sourceApplication] as? [String: AnyObject] {
                
            }
            if let _ = options[UIApplication.LaunchOptionsKey.url] as? [String: AnyObject] {
                
            }
            if let _ = options[UIApplication.LaunchOptionsKey.userActivityDictionary] as? [String: AnyObject] {
                
            }
            if let _ = options[UIApplication.LaunchOptionsKey.userActivityType] as? [String: AnyObject] {
                
            }
        }
    }
    
}

