//
//  RankingViewController.swift
//  Covid-19
//
//  Created by Gonzalo Sanchez on 05/04/2020.
//  Copyright © 2020 eCloudSolutions. All rights reserved.
//

import UIKit
import SwiftExtension
import SearchTextField

class RankingViewController: COBEATBaseViewController {
    
    @IBOutlet private var tableView: UITableView!
    @IBOutlet private var countryField: SearchTextField!
    @IBOutlet private var cityField: SearchTextField!
    @IBOutlet private var countryFieldArrow: UIImageView!
    @IBOutlet private var cityFieldArrow: UIImageView!
    @IBOutlet private var legendCollaboratorsImage: UIImageView!
    @IBOutlet private var legendCollaboratorsLabel: UILabel!
    @IBOutlet private var legendInfectedImage: UIImageView!
    @IBOutlet private var legendInfectedLabel: UILabel!
    
    var rootDelegate: RootViewControllerDelegate?
    private var placePresenter = PlacePresenter<RankingViewController>()
    
    private var countries = [CountryModel]() {
        didSet {
            /// Create 'All' city in order to send null cityID in service and get al cities
            for country in countries {
                let allCities = CityModel()
                allCities.name = R.localizable.allCities()
                country.cities?.insert(allCities, at: 0)
            }
            countryField.filterStrings(countries.map({ ($0.name ?? "") }))
            if countries.count > 0 {
                selectedCountry = countries[0]
            }
            countryField.stopLoadingIndicator()
            cityField.stopLoadingIndicator()
            countryFieldArrow.isHidden = false
            cityFieldArrow.isHidden = false
        }
    }
    private var selectedCountry: CountryModel? {
        didSet {
            countryField.text = selectedCountry?.name
            countryField.resignFirstResponder()
            if let country = selectedCountry, let cities = country.cities {
                cityField.filterStrings(cities.map({ ($0.name ?? "") }))
                selectedCity = country.cities?[0]
            }
            if let optional = selectedCountry?.id {
                placePresenter.getCityRanking(forCountry: String(optional))
            }
        }
    }
    private var selectedCity: CityModel? {
        didSet {
            cityField.text = selectedCity?.name
            cityField.resignFirstResponder()
            filterDatasource()
        }
    }
    
    private var rankingDatasource = [RankingModel]() {
        didSet {
            filterDatasource()
        }
    }
    private var rankingDatasourceFiltered = [RankingModel]() {
        didSet {
            tableView.reloadData()
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationItem.setCenterTitle(R.localizable.cityRanking(), color: .cobeatPrimaryPrimary)
        navigationItem.setCloseLeftButton(viewController: self, color: .cobeatPrimaryPrimary)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    // MARK: - Setup functions
    
    private func setup() {
        placePresenter.attach(view: self)
        placePresenter.getCountries()
        legendCollaboratorsImage.image = R.image.iconCollaborator()
        legendCollaboratorsLabel.set(font: .gothamRoundedBook(12), andColor: .cobeatPrimaryPrimary)
        legendInfectedImage.image = R.image.iconInfected()
        legendInfectedLabel.set(font: .gothamRoundedBook(12), andColor: .cobeatRed)
        legendCollaboratorsLabel.text = R.localizable.collaboratorsPin()
        legendInfectedLabel.text = R.localizable.infectedPin()
        
        countryField.set(font: .gothamRoundedBook(12), andColor: .darkGray)
        countryField.rounded(10, withColor: .white)
        countryField.addShadow(color: .black, radious: 8, opacity: 0.3)
        countryField.setPadding(10)
        countryField.clearButtonMode = .whileEditing
        countryField.autocorrectionType = .no
        countryField.maxResultsListHeight = Int(self.view.frame.height / 3)
        countryField.placeholder = R.localizable.selectCountry()
        countryField.startVisible = true
        countryField.theme.bgColor = .white
        countryField.theme.font = .gothamRoundedBook(12)
        countryField.theme.fontColor = .darkGray
        countryField.theme.cellHeight = 40
        countryField.highlightAttributes = [NSAttributedString.Key.font: UIFont.gothamRoundedBook(12),
                                            NSAttributedString.Key.foregroundColor: UIColor.black]
        countryField.showLoadingIndicator()
        countryField.itemSelectionHandler = {item, itemPosition in
            if let index = self.countries.firstIndex(where: { $0.name == item[itemPosition].title }) {
                self.selectedCountry = self.countries[index]
            }
        }
        
        cityField.set(font: .gothamRoundedBook(12), andColor: .darkGray)
        cityField.rounded(10, withColor: .white)
        cityField.addShadow(color: .black, radious: 8, opacity: 0.3)
        cityField.setPadding(10)
        cityField.clearButtonMode = .whileEditing
        cityField.autocorrectionType = .no
        cityField.maxResultsListHeight = Int(self.view.frame.height / 3)
        cityField.placeholder = R.localizable.selectCity()
        cityField.startVisible = true
        cityField.theme.bgColor = .white
        cityField.theme.font = .gothamRoundedBook(12)
        cityField.theme.fontColor = .darkGray
        cityField.theme.cellHeight = 40
        cityField.highlightAttributes = [NSAttributedString.Key.font: UIFont.gothamRoundedBook(12),
                                            NSAttributedString.Key.foregroundColor: UIColor.black]
        cityField.showLoadingIndicator()
        cityField.itemSelectionHandler = {item, itemPosition in
            if let index = (self.selectedCountry?.cities ?? []).firstIndex(where: { $0.name == item[itemPosition].title }) {
                self.selectedCity = (self.selectedCountry?.cities ?? [])[index]
            }
        }
        
        countryFieldArrow.image = R.image.iconAngleDown()?.with(color: .lightGray)
        cityFieldArrow.image = R.image.iconAngleDown()?.with(color: .lightGray)
        countryFieldArrow.isHidden = true
        cityFieldArrow.isHidden = true
    }
    
    private func filterDatasource() {
        if let id = selectedCity?.id, id > 0 {
            rankingDatasourceFiltered = rankingDatasource.filter({ $0.id == id })
        } else {
            rankingDatasourceFiltered = rankingDatasource
        }
    }
    
    override func closeRightButton() {
        self.dismiss(animated: true)
    }
    
    // MARK: - Override

    override func keyboardWillShow(_ keyboardHeight: CGFloat) {
        
    }
    
    override func keyboardWillHide(_ keyboardHeight: CGFloat) {
        countryFieldArrow.superview?.bringSubviewToFront(countryFieldArrow)
        cityFieldArrow.superview?.bringSubviewToFront(cityFieldArrow)
    }
    
    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {

    }

}

extension RankingViewController: UITextFieldDelegate {
    
}

extension RankingViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return rankingDatasourceFiltered.count
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return CGFloat.zero
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return nil
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return nil
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.reusableCell(withIdentifier: RankingTableViewCell.self)
        let isPair = (indexPath.row % 2 == 0)
        cell.setup(withItem: rankingDatasourceFiltered[indexPath.row], backgroundColor: isPair ? .white : UIColor.lightGray.withAlphaComponent(0.07))
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return RankingTableViewCell.height
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
        return false
    }
}

extension RankingViewController: PlacePresenterDelegate {
    
    func initService(type: DLService) {
        LoadingView.show()
    }
    
    func finishService(type: DLService) {
        LoadingView.dismiss()
    }
    
    func onError(message: String?) {
        LoadingView.dismiss()
        SEAlert.show(title: R.localizable.ups(), message: message ?? "", buttons: (R.localizable.ok(), UIAlertAction.Style.default, { }))
    }
    
    func onGetCountries(_ countries: [CountryModel]) {
        self.countries = countries
    }

    func onGetCity(ranking: [RankingModel]) {
        rankingDatasource = ranking
    }

}

