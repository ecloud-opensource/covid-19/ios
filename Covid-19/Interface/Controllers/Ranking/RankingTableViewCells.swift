//
//  RankingTableViewCells.swift
//  Covid-19
//
//  Created by Gonzalo Sanchez on 10/04/2020.
//  Copyright © 2020 eCloudSolutions. All rights reserved.
//

import UIKit

class RankingTableViewCell: UITableViewCell {
    
    static let height: CGFloat = 55
    
    @IBOutlet private var titleLabel: UILabel!
    @IBOutlet private var collaboratorsIndicator: UIView!
    @IBOutlet private var infectedIndicator: UIView!
    @IBOutlet private var collaboratorsLabel: UILabel!
    @IBOutlet private var infectedLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    // MARK: - Functions
    
    func setup(withItem item: RankingModel, backgroundColor: UIColor) {
        contentView.backgroundColor = backgroundColor
        titleLabel.text = item.name
        collaboratorsLabel.text = "\(item.fellows ?? 0)"
        infectedLabel.text = "\(item.areInfected ?? 0)"
    }
    
    // MARK: - Private unctions
    
    private func setup() {
        titleLabel.set(font: .gothamRoundedBook(14), andColor: .black)
        collaboratorsLabel.set(font: .gothamRoundedBook(14), andColor: .black)
        infectedLabel.set(font: .gothamRoundedBook(14), andColor: .black)
        collaboratorsIndicator.backgroundColor = .cobeatPrimaryPrimary
        infectedIndicator.backgroundColor = .cobeatRed
    }
}
