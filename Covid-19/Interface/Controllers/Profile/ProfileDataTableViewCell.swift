//
//  ProfileTableViewCell.swift
//  Covid-19
//
//  Created by Gonzalo Sanchez on 07/04/2020.
//  Copyright © 2020 eCloudSolutions. All rights reserved.
//

import UIKit

protocol FieldResignDelegate {
    func setFirstResponder()
    func showErrorLabel(_ message: String)
}

class ProfileDataTableViewCell: UITableViewCell, FieldResignDelegate {
    static var height: CGFloat = 80
    
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var textfield: TextfieldWithError!
    
    private var returnHandler: ((ProfileDatasource, Bool)->())?
    private var placesHandler: (()->())?
    private var currentItem = ProfileDatasource(type: .firstname)
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    // MARK: - Functions
    
    func setup(item: ProfileDatasource) {
        currentItem = item
        titleLabel.text = currentItem.type.title
        textfield.placeholder = currentItem.type.placeholder
        textfield.text = currentItem.value as? String
    }
    
    func setPlacesHandler(handler: @escaping ()->()) {
        self.placesHandler = handler
    }
    
    func setReturnHandler(handler: @escaping (ProfileDatasource, Bool)->()) {
        self.returnHandler = handler
    }
    
    // MARK: - Private functions
    
    private func setup() {
        titleLabel.font = .gothamRoundedBook(10)
        titleLabel.textColor = .lightGray
        textfield.font = .gothamRoundedBook(16)
        textfield.textColor = .cobeatPrimaryPrimary
        textfield.underlinedForError(color: .lightGray)
        textfield.setToolbar(target: self,
                             rightSelector: #selector(nextField), rightTitle: R.localizable.continue(),
                             leftSelector: #selector(textfieldDismissed), leftTitle: R.localizable.dismissKeyboard())
    }
    
    @objc func nextField() {
        textfield.resignFirstResponder()
        executeHandler(value: textfield.text ?? "", setFirstResponder: true)
    }
    
    @objc func textfieldDismissed() {
        textfield.resignFirstResponder()
        executeHandler(value: textfield.text ?? "", setFirstResponder: false)
    }
    
    private func executeHandler(value: String, setFirstResponder: Bool) {
        currentItem.value = value
        returnHandler?(currentItem, setFirstResponder)
    }
    
    // MARK: - ContactResignDelegate

    func setFirstResponder() {
        textfield.becomeFirstResponder()
    }
    
    func showErrorLabel(_ message: String) {
        textfield.showErrorLabel(message)
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + .seconds(2)) {
            self.textfield.hideErrorLabel()
        }
    }

}

extension ProfileDataTableViewCell: UITextFieldDelegate {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if currentItem.type == .address {
            placesHandler?()
            return false
        }
        if currentItem.type == .phone {
            return false
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        nextField()
        return true
    }

}
