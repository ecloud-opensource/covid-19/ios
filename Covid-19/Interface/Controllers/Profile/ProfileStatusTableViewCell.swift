//
//  ProfileStatusTableViewCell.swift
//  Covid-19
//
//  Created by Gonzalo Sanchez on 29/04/2020.
//  Copyright © 2020 eCloudSolutions. All rights reserved.
//

import UIKit

class ProfileStatusTableViewCell: UITableViewCell {
    
    static var height: CGFloat = 80
    
    @IBOutlet private var statusImage: UIImageView!
    @IBOutlet private var statusTitle: UILabel!
    @IBOutlet private var statusUpdate: UIButton!

    private var updateStatusCallback: (()->Void)?

    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    // MARK: - Functions

    func setup(withStatus status: UserStatus?, updateStatusCallback: @escaping () -> Void) {
        #warning("Las imágenes tienen muchísima sombra, por eso se ven chiquitas")
        statusImage.image = status?.image
        statusTitle.text = status?.title
        self.updateStatusCallback = updateStatusCallback
    }
    
    // MARK: - Private functions

    private func setup() {
        statusTitle.set(font: .gothamRoundedBook(17), andColor: .cobeatPrimaryPrimary)
        statusUpdate.setTitle(R.localizable.update(), color: .cobeatPrimaryPrimary, font: .gothamRoundedMedium(17))
    }
    
    @IBAction private func updateStatus() {
        updateStatusCallback?()
    }

}
