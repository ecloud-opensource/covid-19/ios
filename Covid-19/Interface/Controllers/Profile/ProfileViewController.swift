//
//  ProfileViewController.swift
//  Covid-19
//
//  Created by Gonzalo Sanchez on 05/04/2020.
//  Copyright © 2020 eCloudSolutions. All rights reserved.
//

import UIKit
import SwiftExtension
import GooglePlaces

class ProfileViewController: COBEATBaseViewController {
    
    @IBOutlet private var tableView : UITableView!
    @IBOutlet private var tableViewBottom : NSLayoutConstraint!
    @IBOutlet private var continueButtonContainer: UIView!
    @IBOutlet private var continueButton: UIButton!
    
    var rootDelegate: RootViewControllerDelegate?
    private var userPresenter = UserPresenter<ProfileViewController>()
    private var datasource: [ProfileDatasource] {
        return userPresenter.profileDatasource
    }
    private var footerButtonContainerHeight: CGFloat {
        return continueButtonContainer.frame.height
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationItem.setCenterTitle(R.localizable.profile(), color: .cobeatPrimaryPrimary)
        navigationItem.setCloseLeftButton(viewController: self, color: .cobeatPrimaryPrimary)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    // MARK: - Setup functions
    
    private func setup() {
        userPresenter.attach(view: self)
        continueButton.setTitle(R.localizable.continue(), color: .white, font: UIFont.gothamRoundedBook(17))
        continueButton.rounded(10, withColor: .cobeatPrimaryPrimary)
        continueButton.addShadow(color: .black, opacity: 0.3)
    }
    
    override func closeRightButton() {
        self.dismiss(animated: true)
    }
    
    // MARK: - Private functions
    
    private func selectAddressFromPlaces() {
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        autocompleteController.modalPresentationStyle = .overFullScreen
        // Specify the place data types to return.
        let fields: GMSPlaceField = GMSPlaceField(rawValue:
            UInt(GMSPlaceField.name.rawValue) |
                UInt(GMSPlaceField.placeID.rawValue) |
                UInt(GMSPlaceField.coordinate.rawValue) |
                GMSPlaceField.addressComponents.rawValue |
                GMSPlaceField.formattedAddress.rawValue)!
        autocompleteController.placeFields = fields
        // Specify a filter.
        let filter = GMSAutocompleteFilter()
        filter.type = .address
        autocompleteController.autocompleteFilter = filter
        // Display the autocomplete view controller.
        present(autocompleteController, animated: true, completion: nil)
    }
    
    // MARK: -  Callback functions
    
    func set(address: GMSPlace) {
        userPresenter.editingUser?.safehouse = PointModel(latitude: address.coordinate.latitude, longitude: address.coordinate.longitude)
        userPresenter.editingUser?.address = address.addressComponents?.first(where: { $0.types[0] == GMSPlacesComponent.address.value })?.name
        userPresenter.editingUser?.number = address.addressComponents?.first(where: { $0.types[0] == GMSPlacesComponent.addressNumber.value })?.name
        for item in datasource where item.type == .address {
            item.value = userPresenter.editingUser?.fullAddress
        }
    }
    
    func set(updatedData data: ProfileDatasource) {
        if let index = datasource.firstIndex(where: { $0.type == data.type }) {
            datasource[index].value = data.value
            switch data.type {
            case .empty, .footer, .address, .phone, .status:
                break
            case .firstname:
                userPresenter.editingUser?.firstName = data.value
            case .lastname:
                userPresenter.editingUser?.lastName = data.value
            case .email:
                userPresenter.editingUser?.email = data.value
            case .identification:
                userPresenter.editingUser?.identification = data.value
            }
        }
    }
    
    func becomeFirstResponder(fromLastIndexPath index: IndexPath) {
        let newIndex = IndexPath(row: index.row + 1, section: 0)
        if newIndex.row < datasource.count {
            if datasource[newIndex.row].type == .address {
                selectAddressFromPlaces()
                return
            }
            tableView.scrollToRow(at: newIndex, at: .top, animated: true)
            if let cell = tableView.cellForRow(at: newIndex) as? FieldResignDelegate {
                cell.setFirstResponder()
            }
        }
    }
    
    func updateStatus() {
        #warning("Call Get user service after questionnaire is updated")
        performSegue(withIdentifier: R.segue.profileViewController.showWebView.identifier, sender: COBEATUIUtils.quizURL())
    }
    
    // MARK: - Override
    
    override func keyboardWillShow(_ keyboardHeight: CGFloat) {
        tableViewBottom.constant = keyboardHeight
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
    }
    
    override func keyboardWillHide(_ keyboardHeight: CGFloat) {
        tableViewBottom.constant = 0
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
    }
    
    // MARK: - IBAction
    
    @IBAction private func onContinueTapped() {
        userPresenter.updateUseService()
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let navController = segue.destination as? UINavigationController, let controller = navController.root as? WebViewController, let url = sender as? String {
            controller.rootDelegate = rootDelegate
            controller.url = url
        }
        
    }
    
}

extension ProfileViewController: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return datasource.count
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return nil
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return nil
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let item = datasource[indexPath.row]
        switch item.type {
        case .empty:
            return ProfileDataTableViewCell.height / 2
        case .footer:
            return footerButtonContainerHeight
        case .firstname, .lastname, .email, .address, .phone, .identification:
            return ProfileDataTableViewCell.height
        case .status:
            return ProfileStatusTableViewCell.height
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = datasource[indexPath.row]
        switch item.type {
        case .empty:
            let cell = UITableViewCell()
            cell.backgroundColor = .clear
            return cell
        case .footer:
            let cell = UITableViewCell()
            cell.backgroundColor = .clear
            return cell
        case .firstname, .lastname, .email, .address, .phone, .identification:
            let cell = tableView.reusableCell(withIdentifier: ProfileDataTableViewCell.self)
            cell.setup(item: item)
            cell.setReturnHandler { (updated, setNextResponder) in
                self.set(updatedData: updated)
                if setNextResponder {
                    self.becomeFirstResponder(fromLastIndexPath: indexPath)
                }
            }
            cell.setPlacesHandler {
                self.selectAddressFromPlaces()
            }
            return cell
        case .status:
            let cell = tableView.reusableCell(withIdentifier: ProfileStatusTableViewCell.self)
            cell.setup(withStatus: UserStatus(optValue: item.value), updateStatusCallback: updateStatus)
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
        return false
    }
    
}

extension ProfileViewController: GMSAutocompleteViewControllerDelegate {
    
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        set(address: place)
        tableView.reloadData()
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        debugLog("GMSAutocompleteViewControllerDelegateError: \(error.localizedDescription)")
    }
    
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
}

extension ProfileViewController: UserPresenterDelegate {
    
    func initService(type: DLService) {
        LoadingView.show()
    }
    
    func finishService(type: DLService) {
        LoadingView.dismiss()
    }
    
    func onError(message: String?) {
        LoadingView.dismiss()
        SEAlert.show(title: R.localizable.ups(), message: message ?? "", buttons: (R.localizable.ok(), UIAlertAction.Style.default, { }))
    }
    
    func showTextfieldsErrors(_ textfields: [TextfieldType]) {
        for field in textfields {
            switch field {
            case .telephone:
                break
            case .verificationPIN:
                break
            case .firstName:
                if let index = datasource.firstIndex(where: { $0.type == ProfileCellType.firstname }),
                    let cell = tableView.cellForRow(at: IndexPath(row: index, section: 0)) as? FieldResignDelegate {
                    cell.showErrorLabel(R.localizable.checkEnteredData())
                }
            case .lastName:
                if let index = datasource.firstIndex(where: { $0.type == ProfileCellType.lastname }),
                    let cell = tableView.cellForRow(at: IndexPath(row: index, section: 0)) as? FieldResignDelegate {
                    cell.showErrorLabel(R.localizable.checkEnteredData())
                }
            case .email:
                if let index = datasource.firstIndex(where: { $0.type == ProfileCellType.email }),
                    let cell = tableView.cellForRow(at: IndexPath(row: index, section: 0)) as? FieldResignDelegate {
                    cell.showErrorLabel(R.localizable.checkEnteredData())
                }
            case .address:
                break
            case .identification:
                if let index = datasource.firstIndex(where: { $0.type == ProfileCellType.identification }),
                    let cell = tableView.cellForRow(at: IndexPath(row: index, section: 0)) as? FieldResignDelegate {
                    cell.showErrorLabel(R.localizable.checkEnteredData())
                }
            }
        }
    }
    
    func onGetUserMe() {
        
    }
    
    func onPutUserMe() {
        SEAlert.show(title: R.localizable.congratulations(), message: R.localizable.userEdited(), buttons: (R.localizable.ok(), UIAlertAction.Style.default, { }))
        userPresenter.fetchUserDatasource()
        tableView.reloadData()
    }
    
}
