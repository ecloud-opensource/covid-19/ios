//
//  AuthPhoneValidationCollectionViewCell.swift
//  Covid-19
//
//  Created by Gonzalo Sanchez on 30/03/2020.
//  Copyright © 2020 eCloudSolutions. All rights reserved.
//

import Foundation
import UIKit
import FlagPhoneNumber

class AuthPhoneValidationCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet private var contenview: UIView!
    @IBOutlet private var phoneTitle: UILabel!
    @IBOutlet private var phoneField: FPNTextField!
    @IBOutlet private var informationLabel: UILabel!
    @IBOutlet private var continueButton: UIButton!
    @IBOutlet private var termsButton: UIButton!
    
    private var onTermsTappedCallback: (()->Void)?
    private var onContinueCallback: ((String?)->Void)?

    private let underlined: [NSAttributedString.Key: Any] = [
        .font: UIFont.gothamRoundedBook(12),
        .foregroundColor: UIColor.lightGray,
        .underlineStyle: NSUnderlineStyle.single.rawValue]
    
    private var enteredPhone: String? {
        didSet {
            setButton()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }
    
    // MARK: - Visible functions
    
    func onTermsTappedCallback(_ callback: @escaping ()->Void) {
        self.onTermsTappedCallback = callback
    }
    
    func onContinueTappedCallback(_ callback: @escaping (String?)->Void) {
        self.onContinueCallback = callback
    }
    
    // MARK: - Private functions
    
    private func setup() {
        contenview.roundBorders(value: 25)
        contenview.backgroundColor = .white
        contenview.addShadow(color: .black, radious: 10, opacity: 0.7)
        phoneTitle.set(font: UIFont.gothamRoundedBook(13), andColor: .cobeatPrimaryPrimary)
        phoneTitle.text = R.localizable.phoneNumber()
        phoneField.set(font: UIFont.gothamRoundedLight(17), andColor: .black)
        phoneField.roundedField(10, withColor: UIColor.black.withAlphaComponent(0.07))
        phoneField.delegate = self
        phoneField.hasPhoneNumberExample = true
        phoneField.flagButtonSize = CGSize(width: 60, height: 35)
        phoneField.setFlag(countryCode: FPNCountryCode.AR)
        phoneField.textFieldInputAccessoryView = setFieldInputView()
        phoneField.autocorrectionType = .no
        phoneField.displayMode = .picker
        informationLabel.set(font: UIFont.gothamRoundedBook(11), andColor: .lightGray)
        informationLabel.text = R.localizable.codeWillBeSent()
        continueButton.setTitle(R.localizable.continue(), color: .white, font: UIFont.gothamRoundedBook(17))
        continueButton.rounded(10, withColor: .cobeatPrimaryPrimary)
        continueButton.addShadow(color: .black, opacity: 0.3)
        setButton()
        let underlinedString = NSMutableAttributedString(string: R.localizable.acceptTerms(),  attributes: underlined)
        termsButton.setAttributedTitle(underlinedString, for: .normal)
    }
    
    private func setFieldInputView() -> UIToolbar {
        let okButton = UIBarButtonItem(title: R.localizable.ok(), style: .done, target: self, action: #selector(dismissFields))
        let toolbar: UIToolbar = UIToolbar()
        toolbar.barStyle = UIBarStyle.default
        toolbar.items = [okButton]
        toolbar.tintColor = .darkGray
        toolbar.sizeToFit()
        return toolbar
    }
    
    @objc private func dismissFields() {
        phoneField.resignFirstResponder()
    }
    
    private func setButton() {
        guard let phone = enteredPhone, phone.isNotEmpty else {
            continueButton.isEnabled = false
            continueButton.alpha = 0.5
            return
        }
        continueButton.isEnabled = true
        continueButton.alpha = 1
    }
    
    // MARK: - IBActions

    @IBAction private func onTermsTapped() {
        onTermsTappedCallback?()
    }
    
    @IBAction private func onContinueTapped() {
        onContinueCallback?(enteredPhone)
    }
    
}

extension AuthPhoneValidationCollectionViewCell: FPNTextFieldDelegate {

    func fpnDidSelectCountry(name: String, dialCode: String, code: String) {

    }
    
    func fpnDidValidatePhoneNumber(textField: FPNTextField, isValid: Bool) {
        if isValid {
            textField.resignFirstResponder()
            enteredPhone = textField.getFormattedPhoneNumber(format: .E164)
        } else {
            enteredPhone = nil
        }
    }
    
    func fpnDisplayCountryList() {
        
    }
}

extension AuthPhoneValidationCollectionViewCell: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let text = ((textField.text ?? "") as NSString).replacingCharacters(in: range, with: string)
        let _ = text.count
        return true
    }
    
}
