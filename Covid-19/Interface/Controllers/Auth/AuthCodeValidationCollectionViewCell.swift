//
//  AuthCodeValidationCollectionViewCell.swift
//  Covid-19
//
//  Created by Gonzalo Sanchez on 30/03/2020.
//  Copyright © 2020 eCloudSolutions. All rights reserved.
//

import Foundation
import UIKit
class AuthCodeValidationCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet private var contenview: UIView!
    @IBOutlet private var mainTitle: UILabel!
    @IBOutlet private var mainSubtitle: UILabel!
    @IBOutlet private var resendButton: UIButton!
    @IBOutlet private var newPhoneButton: UIButton!
    @IBOutlet private var codeFieldsContent: UIView!
    @IBOutlet private var code1Textfield: EresableTextField!
    @IBOutlet private var code2Textfield: EresableTextField!
    @IBOutlet private var code3Textfield: EresableTextField!
    @IBOutlet private var code4Textfield: EresableTextField!
    @IBOutlet private var code5Textfield: EresableTextField!
    @IBOutlet private var code6Textfield: EresableTextField!
    
    private var onContinueCallback: ((String?)->Void)?
    private var onResendCodeCallback: (()->Void)?
    private var onChangePhoneCallback: (()->Void)?

    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }
    
    // MARK: - Visible functions
    
    func setup(withPhoneNumber number: String) {
        mainSubtitle.text = R.localizable.codeSentTo("\(number)")
    }
    
    func onContinueTappedCallback(_ callback: @escaping (String?)->Void) {
        self.onContinueCallback = callback
    }
    
    func onResendCodeCallback(_ callback: @escaping ()->Void) {
        self.onResendCodeCallback = callback
    }
    
    func onChangePhoneCallback(_ callback: @escaping ()->Void) {
        self.onChangePhoneCallback = callback
    }
    
    func clearFields() {
        code1Textfield.text = nil
        code2Textfield.text = nil
        code3Textfield.text = nil
        code4Textfield.text = nil
        code5Textfield.text = nil
        code6Textfield.text = nil
    }
    
    func activeResendButton() {
        resendButton.isUserInteractionEnabled = true
        resendButton.setTitle(R.localizable.resendCode(), color: .cobeatPrimaryPrimary, font: UIFont.gothamRoundedBook(12))
    }
    
    func inactiveResendButton(_ title: String) {
        resendButton.isUserInteractionEnabled = false
        resendButton.setTitle(title, color: UIColor.black.withAlphaComponent(0.4), font: UIFont.gothamRoundedBook(12))
    }
    
    // MARK: - Private functions
    
    private func setup() {
        contenview.roundBorders(value: 25)
        contenview.backgroundColor = .white
        contenview.addShadow(color: .black, radious: 10, opacity: 0.7)
        mainTitle.set(font: UIFont.gothamRoundedBook(18), andColor: .cobeatPrimaryPrimary)
        mainTitle.text = R.localizable.whichIsTheCode()
        mainSubtitle.set(font: UIFont.gothamRoundedBook(12), andColor: UIColor.black.withAlphaComponent(0.4))
        codeFieldsContent.rounded(10, withColor: .white)
        codeFieldsContent.addShadow(color: .black, radious: 10, opacity: 0.3)
        setupFields()
        inactiveResendButton(R.localizable.resendCode())
        newPhoneButton.setTitle(R.localizable.changePhoneNumber(), color: UIColor.black.withAlphaComponent(0.6), font: UIFont.gothamRoundedBook(12))
    }
    
    private func setupFields() {
        code1Textfield.set(font: UIFont.gothamRoundedLight(17), andColor: .black)
        code1Textfield.underlined(color: .lightGray, lineHeight: 1)
        code1Textfield.borderStyle = .none
        code1Textfield.autocorrectionType = .no
        code1Textfield.eresableTextFieldDelegate = self
        code2Textfield.set(font: UIFont.gothamRoundedLight(17), andColor: .black)
        code2Textfield.underlined(color: .lightGray, lineHeight: 1)
        code2Textfield.borderStyle = .none
        code2Textfield.autocorrectionType = .no
        code2Textfield.eresableTextFieldDelegate = self
        code3Textfield.set(font: UIFont.gothamRoundedLight(17), andColor: .black)
        code3Textfield.underlined(color: .lightGray, lineHeight: 1)
        code3Textfield.borderStyle = .none
        code3Textfield.autocorrectionType = .no
        code3Textfield.eresableTextFieldDelegate = self
        code4Textfield.set(font: UIFont.gothamRoundedLight(17), andColor: .black)
        code4Textfield.underlined(color: .lightGray, lineHeight: 1)
        code4Textfield.borderStyle = .none
        code4Textfield.autocorrectionType = .no
        code4Textfield.eresableTextFieldDelegate = self
        code5Textfield.set(font: UIFont.gothamRoundedLight(17), andColor: .black)
        code5Textfield.underlined(color: .lightGray, lineHeight: 1)
        code5Textfield.borderStyle = .none
        code5Textfield.autocorrectionType = .no
        code5Textfield.eresableTextFieldDelegate = self
        code6Textfield.set(font: UIFont.gothamRoundedLight(17), andColor: .black)
        code6Textfield.underlined(color: .lightGray, lineHeight: 1)
        code6Textfield.borderStyle = .none
        code6Textfield.autocorrectionType = .no
        code6Textfield.eresableTextFieldDelegate = self
    }
    
    private func extractPIN() -> String? {
        guard let value1 = code1Textfield.text, value1.count == 1,
            let value2 = code2Textfield.text, value2.count == 1,
            let value3 = code3Textfield.text, value3.count == 1,
            let value4 = code4Textfield.text, value4.count == 1,
            let value5 = code5Textfield.text, value5.count == 1,
            let value6 = code6Textfield.text, value6.count == 1 else {
                return nil
        }
        return "\(value1)\(value2)\(value3)\(value4)\(value5)\(value6)"
    }
    
    private func onEnteredCode() {
        onContinueCallback?(extractPIN())
    }
    
    // MARK: - IBAction functions

    @IBAction private func onResendCodeTapped() {
        onResendCodeCallback?()
    }
    
    @IBAction private func onChangePhoneTapped() {
        onChangePhoneCallback?()
    }
    
}

extension AuthCodeValidationCollectionViewCell: UITextFieldDelegate, EresableTextFieldDelegate {
    
    // UITextFieldDelegate
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let text = ((textField.text ?? "") as NSString).replacingCharacters(in: range, with: string)
        let textCount = text.count
        
        if textCount <= 1 {
            switch textField {
            case code1Textfield:
                if text.isValidNumber() || text == "" {
                    code1Textfield.text = text
                    if !(text == "") {
                        code2Textfield.becomeFirstResponder()
                    }
                }
            case code2Textfield:
                if text.isValidNumber() || text == "" {
                    code2Textfield.text = text
                    if !(text == "") {
                        code3Textfield.becomeFirstResponder()
                    }
                }
            case code3Textfield:
                if text.isValidNumber() || text == "" {
                    code3Textfield.text = text
                    if !(text == "") {
                        code4Textfield.becomeFirstResponder()
                    }
                }
            case code4Textfield:
                if text.isValidNumber() || text == "" {
                    code4Textfield.text = text
                    if !(text == "") {
                        code5Textfield.becomeFirstResponder()
                    }
                }
            case code5Textfield:
                if text.isValidNumber() || text == "" {
                    code5Textfield.text = text
                    if !(text == "") {
                        code6Textfield.becomeFirstResponder()
                    }
                }
            case code6Textfield:
                if text.isValidNumber() || text == "" {
                    code6Textfield.text = text
                    if !(text == "") {
                        code6Textfield.resignFirstResponder()
                        onEnteredCode()
                    }
                }
            default:
                break
            }
        }
        
        return false
    }
    
    // EresableTextFieldDelegate
    
    func textfieldDidEnterBackward(_ textield: UITextField) {
        switch textield {
        case code1Textfield:
            break
        case code2Textfield:
            code1Textfield.becomeFirstResponder()
        case code3Textfield:
            code2Textfield.becomeFirstResponder()
        case code4Textfield:
            code3Textfield.becomeFirstResponder()
        case code5Textfield:
            code4Textfield.becomeFirstResponder()
        case code6Textfield:
            code5Textfield.becomeFirstResponder()
        default:
            break
        }
    }
    
}

