//
//  AuthViewController.swift
//  Covid-19
//
//  Created by Gonzalo Sanchez on 30/03/2020.
//  Copyright © 2020 eCloudSolutions. All rights reserved.
//

import UIKit

import UIKit
import SwiftExtension


class AuthViewController: COBEATBaseViewController {
    
    @IBOutlet private var collectionView: UICollectionView!
    @IBOutlet private var contentBottomConstraint: NSLayoutConstraint!
    
    private var authPresenter = AuthPresenter<AuthViewController>()
    var rootDelegate: RootViewControllerDelegate?
    var controllerType = ReusableControllerType.undismisssable
    
    private let datasource = [AuthCellType.phoneValidation, AuthCellType.codeValidation]
    private var phoneValidationCell: AuthPhoneValidationCollectionViewCell?
    private var codeValidationCell: AuthCodeValidationCollectionViewCell?

    private enum AuthCellType {
        case phoneValidation
        case codeValidation
    }
        
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        checkControllerType()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        collectionView.reloadData()
    }
    
    // MARK: - Override
    
    override func keyboardWillShow(_ keyboardHeight: CGFloat) {
        contentBottomConstraint.constant = keyboardHeight * 0.7
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
    }
    
    override func keyboardWillHide(_ keyboardHeight: CGFloat) {
        contentBottomConstraint.constant = 0
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
    }
    
    // MARK: - Functions
    
    private func setup() {
        view.backgroundColor = .white
        authPresenter.attach(view: self)
    }
    
    private func checkControllerType() {
        switch controllerType {
        case .drawer:
            navigationController?.navigationBar.setNavigationBarTranslucent()
            navigationItem.setLeftMenuButton(viewController: self)
        case .helper:
            navigationController?.navigationBar.setNavigationBarTranslucent()
            navigationItem.setCloseMenuButton(viewController: self)
        case .undismisssable:
            navigationController?.setNavigationBarHidden(true, animated: true)
        }
    }
    
    private func onValidatePhoneTapped(phone: String?) {
        authPresenter.set(phone: phone)
    }
    
    private func onValidateCodeTapped(code: String?) {
        authPresenter.set(code: code)
    }
    
    private func onResendCodeTapped() {
        authPresenter.resendCode()
    }
    
    private func onChangePhoneTapped() {
        collectionView.scrollToItem(at: IndexPath(row: 0, section: 0), at: .centeredHorizontally, animated: true)
    }
    
    private func checkEnteredCodeError() {
        authPresenter.clearCode()
        codeValidationCell?.clearFields()
    }
    
    // MARK: - Navigation
    
    private func showTerms() {
        authPresenter.showTerms()
    }
    
    private func showCodeValidation() {
        collectionView.reloadData()
        collectionView.scrollToItem(at: IndexPath(row: 1, section: 0), at: .centeredHorizontally, animated: true)
    }
    
    private func showHome() {
        rootDelegate?.showHome(nil)
    }
    
}

extension AuthViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UIScrollViewDelegate {
    
    // MARK: - UIScrollViewDelegate
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
    }
    
    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
        
    }
    
    // MARK: - UICollectionViewDataSource
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return datasource.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch datasource[indexPath.row] {
        case .phoneValidation:
            if phoneValidationCell == nil {
                phoneValidationCell = collectionView.reusableCell(withIdentifier: AuthPhoneValidationCollectionViewCell.self, for: indexPath)
            }
            phoneValidationCell?.onTermsTappedCallback {
                self.showTerms()
            }
            phoneValidationCell?.onContinueTappedCallback({ (phone) in
                self.onValidatePhoneTapped(phone: phone)
            })
            return phoneValidationCell ?? UICollectionViewCell()
        case .codeValidation:
            if codeValidationCell == nil {
                codeValidationCell = collectionView.reusableCell(withIdentifier: AuthCodeValidationCollectionViewCell.self, for: indexPath)
            }
            codeValidationCell?.setup(withPhoneNumber: authPresenter.enteredPhone)
            codeValidationCell?.onContinueTappedCallback({ (code) in
                self.onValidateCodeTapped(code: code)
            })
            codeValidationCell?.onResendCodeCallback {
                self.onResendCodeTapped()
            }
            codeValidationCell?.onChangePhoneCallback {
                self.onChangePhoneTapped()
            }
            return codeValidationCell ?? UICollectionViewCell()
        }
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    // MARK: - UICollectionViewDelegateFlowLayout
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: collectionView.frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.zero
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        return CGSize()
    }
    
    // MARK: - UICollectionViewDelegate
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
    
}

extension AuthViewController: BasePresenterDelegate {
    
    func initService(type: DLService) {
        LoadingView.show()
    }
    
    func finishService(type: DLService) {
        LoadingView.dismiss()
    }
    
    func onError(message: String?) {
        checkEnteredCodeError()
        LoadingView.dismiss()
        SEAlert.show(title: R.localizable.ups(), message: message ?? "", buttons: (R.localizable.ok(), UIAlertAction.Style.default, { }))
    }
    
}


extension AuthViewController: AuthPresenterDelegate {

    func onPostPhoneValidation() {
        authPresenter.startSMSTimer()
        updateSMSTimer(R.localizable.resendCode())
        showCodeValidation()
    }
    
    func onPostCodeValidation() {
        /// After onPostCodeValidation user is saved and then
        /// onGetUserMe is called.
        /// showHome is shown in onGetUserMe
    }
    
    func onGetUserMe() {
         showHome()
    }
    
    func updateSMSTimer(_ value: String?) {
        guard let title = value else {
            codeValidationCell?.activeResendButton()
            return
        }
        codeValidationCell?.inactiveResendButton(title)
    }
    
}


