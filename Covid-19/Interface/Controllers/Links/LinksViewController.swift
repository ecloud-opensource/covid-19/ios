//
//  LinksViewController.swift
//  
//
//  Created by Gonzalo Sanchez on 05/04/2020.
//

import UIKit
import SwiftExtension

class LinksViewController: COBEATBaseViewController {
    
    @IBOutlet private var tableView: UITableView!
    
    var rootDelegate: RootViewControllerDelegate?
    private var placePresenter = PlacePresenter<LinksViewController>()
    private var city: CityModel?
    private var country: CountryModel?

    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationItem.setCenterTitle(R.localizable.links(), color: .cobeatPrimaryPrimary)
        navigationItem.setCloseLeftButton(viewController: self, color: .cobeatPrimaryPrimary)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    // MARK: - Setup functions
    
    private func setup() {
        placePresenter.attach(view: self)
        placePresenter.getCityLinks()
    }
    
    override func closeRightButton() {
        self.dismiss(animated: true)
    }
    
    private func show(link: String?) {
        COBEATUIUtils.openInternalBrowser(withStringURL: link ?? "")
    }
    
    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {

    }

}

extension LinksViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return country?.link?.count ?? 1
        } else if section == 1 {
            return city?.link?.count ?? 1
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return LinkTitleTableViewCell.height
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.reusableCell(withIdentifier: LinkTitleTableViewCell.self)
        if section == 0 {
            cell.setup(withTitle: country?.name ?? placePresenter.currentUserCountry)
        } else if section == 1 {
            cell.setup(withTitle: city?.name ?? placePresenter.currentUserCityName)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return nil
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.reusableCell(withIdentifier: LinkDetailTableViewCell.self)
        let title = R.localizable.title()
        let subtitle = R.localizable.subtitle()
        if indexPath.section == 0 {
            if country?.link?.isEmpty ?? true {
                cell.setupEmpty()
            } else {
                cell.setup(withTitle: country?.link?[indexPath.row].title ?? title,
                           andSubtitle: (country?.link?[indexPath.row].info ?? subtitle).uppercased())
            }
        } else if indexPath.section == 1 {
            if city?.link?.isEmpty ?? true {
                cell.setupEmpty()
            } else {
                cell.setup(withTitle: city?.link?[indexPath.row].title ?? title,
                           andSubtitle: (city?.link?[indexPath.row].info ?? subtitle).uppercased())
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return LinkDetailTableViewCell.height
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
        var link: String?
        if indexPath.section == 0 {
            link = country?.link?[indexPath.row].url
        } else if indexPath.section == 1 {
            link = city?.link?[indexPath.row].url
        }
        show(link: link)
        return false
    }
    
}

extension LinksViewController: PlacePresenterDelegate {
    
    func initService(type: DLService) {
        LoadingView.show()
    }
    
    func finishService(type: DLService) {
        LoadingView.dismiss()
    }
    
    func onError(_ message: String?) {
        LoadingView.dismiss()
        SEAlert.show(title: R.localizable.ups(), message: message ?? "", buttons: (R.localizable.ok(), UIAlertAction.Style.default, { }))
    }
    
    func onGetCityLink(city: CityModel, country: CountryModel) {
        self.city = city
        self.country = country
        tableView.reloadData()
    }
    
}
