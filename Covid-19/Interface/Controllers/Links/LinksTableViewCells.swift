//
//  LinksTableViewCells.swift
//  Covid-19
//
//  Created by Gonzalo Sanchez on 08/04/2020.
//  Copyright © 2020 eCloudSolutions. All rights reserved.
//

import UIKit

class LinkTitleTableViewCell: UITableViewCell {
    
    @IBOutlet var titleLabel: UILabel!
    
    static let height: CGFloat = 50
    private var marqueeTimer: Timer?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    // MARK: - Functions
    
    func setup(withTitle title: String) {
        titleLabel.text = title
    }
    
    // MARK: - Private unctions
    
    private func setup() {
        titleLabel.set(font: .gothamRoundedMedium(15), andColor: .black)
    }
}

class LinkDetailTableViewCell: UITableViewCell {
    
    @IBOutlet private var mainContent: UIView!
    @IBOutlet private var titleLabel: UILabel!
    @IBOutlet private var subtitleLabel: UILabel!
    @IBOutlet private var centerLabel: UILabel!

    static let height: CGFloat = 150
    private var marqueeTimer: Timer?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        baseSetup()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    // MARK: - Functions
    
    func setup(withTitle title: String, andSubtitle subtitle: String) {
        mainContent.rounded(10, withColor: .cobeatGrayBackground)
        titleLabel.text = title
        subtitleLabel.text = subtitle
        centerLabel.text = nil
    }
    
    func setupEmpty() {
        mainContent.rounded(10, withColor: .cobeatPrimaryPrimary)
        titleLabel.text = nil
        subtitleLabel.text = nil
        centerLabel.text = R.localizable.noLinksAvailable()
    }
    
    // MARK: - Private functions
    
    private func baseSetup() {
        titleLabel.set(font: .gothamRoundedBook(17), andColor: .darkGray)
        subtitleLabel.set(font: .gothamRoundedMedium(12), andColor: .lightGray)
        centerLabel.set(font: .gothamRoundedBook(15), andColor: UIColor.white.withAlphaComponent(0.5))
        mainContent.addShadow(color: .black, radious: 2, opacity: 0.3)
    }

}

