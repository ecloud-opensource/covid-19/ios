//
//  WebViewController.swift
//  Covid-19
//
//  Created by Gonzalo Sanchez on 25/04/2020.
//  Copyright © 2020 eCloudSolutions. All rights reserved.
//

import UIKit
import WebKit
import SwiftExtension

class WebViewController: COBEATBaseViewController {
    
    @IBOutlet private var webview: WKWebView!

    var rootDelegate: RootViewControllerDelegate?
    var url = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    // MARK: - Functions
    
    private func setup() {
        setCenter(title: R.localizable.questionnaireTitle())
        setCloseButton()
        configureWebView()
    }
    
    private func configureWebView() {
        webview.configuration.userContentController.add(self, name: "setTitle")
        if let url = URL(string: self.url) {
            webview.load(URLRequest(url: url))
        } else {
            BannerView.show(withError: R.localizable.questionnaireUrlError())
        }
    }
    
    private func setCloseButton() {
        if self.navigationController?.presentingViewController != nil {
            navigationItem.setCloseMenuButton(viewController: self, color: .cobeatPrimaryPrimary)
        } else {
            navigationItem.setBackMenuButton(viewController: self, color: .cobeatPrimaryPrimary)
        }
    }

    private func setCenter(title: String) {
        setCOBEATNavigationBar(withTitle: title)
    }

    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {

    }

}

extension WebViewController: WKScriptMessageHandler {

    // WKScriptMessageHandler
    
    func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
        if let contentBody = message.body as? String{

        }
    }
    
}
