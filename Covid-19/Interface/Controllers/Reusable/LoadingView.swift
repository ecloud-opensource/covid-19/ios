//
//  LoadingView.swift
//  Covid-19
//
//  Created by Gonzalo Sanchez on 29/04/2020.
//  Copyright © 2020 eCloudSolutions. All rights reserved.
//

import UIKit
import Lottie

class LoadingView: UIView {
    
    @IBOutlet private var backgroundView: UIView!
    @IBOutlet private var animationView: AnimationView!

    static var instance: LoadingView?
    
    override func awakeAfter(using aDecoder: NSCoder) -> Any? {
        return loadFromNib()
    }
    
    class func show() {
        DispatchQueue.main.async { () -> Void in
            guard let _ = instance else {
                    let view: LoadingView =  initFromNib()
                    instance = view
                    view.frame = UIApplication.shared.keyWindow?.frame ?? CGRect()
                    view.setup()
                    UIApplication.shared.keyWindow?.addSubview(view)
                    instance?.showAnimation()
                    return

            }
            /// Handle loading view updates if loading is already shown
        }
    }
    
    class func dismiss() {
        guard let current = instance else {
            return
        }
        current.dismissAnimation()
    }
    
    private func setup() {
        backgroundView.backgroundColor = UIColor.white.withAlphaComponent(0.8)
        animationView.backgroundColor = .clear
        animationView.addShadow(color: .darkGray, radious: 1, opacity: 1)
        animationView.animation = Animation.named("LoaderAnimation")
        animationView.loopMode = .loop
        animationView.contentMode = .scaleAspectFit
        backgroundView.alpha = 0
        animationView.alpha = 0
    }
    
    private func showAnimation() {
        DispatchQueue.main.async { () -> Void in
            UIView.animate(withDuration: 0.3, animations: {
                self.backgroundView.alpha = 1
            }) { (bool) in
                UIView.animate(withDuration: 0.15, animations: {
                    self.animationView.alpha = 1
                }) { (bool) in
                    self.animationView.play()
                }
            }
        }

    }
    
    private func dismissAnimation() {
        DispatchQueue.main.async { () -> Void in
            UIView.animate(withDuration: 0.15, animations: {
                self.animationView.alpha = 0
            }) { (bool) in
                UIView.animate(withDuration: 0.3, animations: {
                    self.backgroundView.alpha = 0
                }) { (bool) in
                    self.viewDismissed()
                }
            }
        }
    }
    
    private func viewDismissed() {
        removeFromSuperview()
        LoadingView.instance = nil
    }
    
}
