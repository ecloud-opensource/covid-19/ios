//
//  ViewController.swift
//  Covid-19
//
//  Created by Juan Pablo on 28/03/2020.
//  Copyright © 2020 eCloudSolutions. All rights reserved.
//

import UIKit
import SwiftExtension
import GoogleMaps
import GooglePlaces

class HomeViewController: COBEATBaseViewController {
    
    @IBOutlet private var mapView: GMSMapView!
    @IBOutlet private var tableView: UITableView!
    @IBOutlet private var tableTopViewTopConstraint: NSLayoutConstraint!
    @IBOutlet private var informationContentView: UIView!
    @IBOutlet private var informationContentWidth: NSLayoutConstraint!
    @IBOutlet private var informationContentHeight: NSLayoutConstraint!
    @IBOutlet private var informationLabel: UILabel!
    @IBOutlet private var informationButton: UIButton!
    @IBOutlet private var safeHouseContentView: UIView!
    @IBOutlet private var safeHouseContentViewBottom: NSLayoutConstraint!
    @IBOutlet private var safeHouseContentInternal: UIView!
    @IBOutlet private var safeHouseTitle: UILabel!
    @IBOutlet private var safeHouseTextfield: UITextField!
    @IBOutlet private var safeHouseContinueButton: UIButton!
    @IBOutlet private var legendCollaboratorsImage: UIImageView!
    @IBOutlet private var legendCollaboratorsLabel: UILabel!
    @IBOutlet private var legendInfectedImage: UIImageView!
    @IBOutlet private var legendInfectedLabel: UILabel!
    
    var rootDelegate: RootViewControllerDelegate?
    private var homePresenter = HomePresenter<HomeViewController>()
    private var geoPresenter = GeoPresenter<HomeViewController>()
    private var userPresenter = UserPresenter<HomeViewController>()
    private var placePresenter = PlacePresenter<HomeViewController>()
    private var users = [UserModel]()
    private var notifications = [NotificationModel]()
    private var feed = [CityFeedModel]()
    private var disabledUserLocationAlertShown = false
    private var disabledSafehouseAlertShown = false
    private var notificationClosed = false
    private var oldContentOffset = CGPoint.zero
    private var isTapHandled = false
    var totalInfected: Int {
        var infected = 0
        for user in users where user.isQuarantined == true {
            infected += 1
        }
        return infected
    }
    var flapNotificationText: String {
        var string = ""
        for notification in notifications where (notification.isFooter && !(notification.title?.isEmpty ?? true)) {
            string = string.isEmpty ? "\(notification.title!)" : "\(string) | \(notification.title!)"
        }
        return string.isEmpty ? R.localizable.shareApp() : "\(string) |"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationItem.setLeftMenuButton(viewController: self)
        setCOBEATNavigationBarWithLogo()
        setupTableDefaultPosition()
        setupUserSafeHouse()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    // MARK: - Public functions
    
    func present(section: DrawerSectionType) {
        switch section {
        case .header, .footer:
            break
        case .profile:
            performSegue(withIdentifier: R.segue.homeViewController.showProfile, sender: nil)
        case .links:
            performSegue(withIdentifier: R.segue.homeViewController.showLinks, sender: nil)
        case .ranking:
            performSegue(withIdentifier: R.segue.homeViewController.showRanking, sender: nil)
        case .collaborate:
            performSegue(withIdentifier: R.segue.homeViewController.showCollaborate, sender: nil)
        case .terms:
            COBEATUIUtils.openInternalBrowser(withStringURL: COBEATUIUtils.termsURL())
        }
    }
    
    // MARK: - Setup functions
    
    private func setup() {
        homePresenter.attach(view: self)
        geoPresenter.attach(view: self)
        userPresenter.attach(view: self)
        placePresenter.attach(view: self)
        mapView.delegate = self
        setupMapStyle()
        setupInformationView()
        setLegend()
        setupSafeHouseView()
        setupSafeHouseViewDefaultPosition()
        addUserLocationPin()
        centerOnUserLocation(shouldUseSafeHouse: true)
        getCityFeed()
    }
    
    private func setupTableDefaultPosition() {
        tableTopViewTopConstraint.constant = tableDefaultYPosition()
        view.layoutIfNeeded()
        tableView.contentOffset.y = 0
    }
    
    private func setupSafeHouseViewDefaultPosition() {
        safeHouseContentViewBottom.constant = -safeHouseContentView.frame.height
        view.layoutIfNeeded()
    }
    
    private func setupUserSafeHouse() {
        userPresenter.getUserHasEnteredSafehouse ? configureWithSafehouse() : configureWithoutSafehouse()
    }
    
    private func setupSafeHouseView() {
        safeHouseContentView.rounded(10, withColor: .cobeatGrayBackground)
        safeHouseContentView.addShadow(color: UIColor.black.withAlphaComponent(0.5))
        safeHouseContentInternal.rounded(10, withColor: .white)
        safeHouseTitle.set(font: .gothamRoundedBook(17), andColor: .black)
        safeHouseTitle.text = R.localizable.enterSafehouse()
        safeHouseContinueButton.setTitle(R.localizable.collaborateNow(), color: .white, font: UIFont.gothamRoundedBook(17))
        safeHouseContinueButton.rounded(10, withColor: .cobeatPrimaryPrimary)
        safeHouseContinueButton.addShadow(color: .black, opacity: 0.3)
        safeHouseTextfield.set(font: UIFont.gothamRoundedLight(15), andColor: .black)
        safeHouseTextfield.placeholder = R.localizable.enterAddress()
        safeHouseTextfield.roundedField(10, withColor: .white)
        safeHouseTextfield.addShadow(color: .black, radious: 5, opacity: 0.2)
        safeHouseTextfield.setPadding(10)
    }
    
    private func setupMapStyle() {
        mapView.settings.myLocationButton = true
        do {
            if let styleURL = Bundle.main.url(forResource: "MapStyle", withExtension: "json") {
                mapView.mapStyle = try GMSMapStyle(contentsOfFileURL: styleURL)
            } else {
                print("Unable to find style.json")
            }
        } catch {
            print("One or more of the map styles failed to load. \(error)")
        }
    }
    
    private func setupInformationView() {
        func hideInformationViewDefault() {
            informationContentWidth.constant = 0
            informationContentHeight.constant = 0
            informationLabel.alpha = 0
            informationButton.alpha = 0
            informationLabel.isHidden = true
            informationButton.isHidden = true
            view.layoutIfNeeded()
        }
        informationContentView.backgroundColor = UIColor.cobeatOrage.withAlphaComponent(0.8)
        informationContentView.roundBorders(value: 10)
        informationLabel.set(font: .gothamRoundedBook(16), andColor: .white)
        informationLabel.adjustsFontSizeToFitWidth = true
        informationButton.setImage(R.image.iconClose()?.with(color: .white), for: .normal)
        hideInformationViewDefault()
    }
    
    private func setupColaborateButton() {
        guard userPresenter.editingUser?.safehouse != nil else {
            safeHouseContinueButton.isEnabled = false
            safeHouseContinueButton.alpha = 0.5
            return
        }
        safeHouseContinueButton.isEnabled = true
        safeHouseContinueButton.alpha = 1
    }
    
    private func selectAddressFromPlaces() {
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        autocompleteController.modalPresentationStyle = .overFullScreen
        // Specify the place data types to return.
        let fields: GMSPlaceField = GMSPlaceField(rawValue:
            UInt(GMSPlaceField.name.rawValue) |
                UInt(GMSPlaceField.placeID.rawValue) |
                UInt(GMSPlaceField.coordinate.rawValue) |
                GMSPlaceField.addressComponents.rawValue |
                GMSPlaceField.formattedAddress.rawValue)!
        autocompleteController.placeFields = fields
        // Specify a filter.
        let filter = GMSAutocompleteFilter()
        filter.type = .address
        autocompleteController.autocompleteFilter = filter
        // Display the autocomplete view controller.
        present(autocompleteController, animated: true, completion: nil)
    }
    
    private func setLegend() {
        legendCollaboratorsImage.image = R.image.iconCollaborator()
        legendCollaboratorsLabel.set(font: .gothamRoundedBook(11), andColor: .cobeatPrimaryPrimary)
        legendInfectedImage.image = R.image.iconInfected()
        legendInfectedLabel.set(font: .gothamRoundedBook(11), andColor: .cobeatRed)
        showLegendLabels()
    }
    
    private func reloadTableWithNotifications() {
        if let index = homePresenter.infoTableDatasource.firstIndex(of: .flap),
            let cell = tableView.cellForRow(at: IndexPath(row: index, section: 0)) as? HomeFlapTableViewCell {
            if cell.informationLabel.text != flapNotificationText {
                tableView.reloadData()
            }
        }
    }
    
    // MARK: - Callbacks
    
    func set(address: GMSPlace) {
        userPresenter.editingUser?.safehouse = PointModel(latitude: address.coordinate.latitude, longitude: address.coordinate.longitude)
        userPresenter.editingUser?.address = address.addressComponents?.first(where: { $0.types[0] == GMSPlacesComponent.address.value })?.name
        userPresenter.editingUser?.number = address.addressComponents?.first(where: { $0.types[0] == GMSPlacesComponent.addressNumber.value })?.name
    }
    
    // MARK: - IBActions
    
    @IBAction private func onCloseInformationTapped() {
        hideInformationView()
    }
    
    @IBAction private func onSafehouseFieldTapped() {
        safeHouseTextfield.resignFirstResponder()
        selectAddressFromPlaces()
    }
    
    @IBAction private func onColaborateTapped() {
        userPresenter.updateUseService()
    }
    
    // MARK: - Navigation
    
    private func showQuiz() {
        handleFlapTap()
        performSegue(withIdentifier: R.segue.homeViewController.showWebView.identifier, sender: COBEATUIUtils.quizURL())
    }
    
    private func shareAPP() {
        handleFlapTap()
        COBEATUIUtils.share(items: homePresenter.getShareItems(), fromVC: self)
    }
    
    private func callSympton() {
        homePresenter.callSympton()
    }
    
    private func callReport() {
        homePresenter.callReport()
    }
    
    private func showFeedDetail(_ feed: CityFeedModel) {
        COBEATUIUtils.openInternalBrowser(withStringURL: feed.url ?? "")
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let navController = segue.destination as? UINavigationController, let controller = navController.root as? ProfileViewController {
            controller.rootDelegate = rootDelegate
        }
        
        if let navController = segue.destination as? UINavigationController, let controller = navController.root as? LinksViewController {
            controller.rootDelegate = rootDelegate
        }
        
        if let navController = segue.destination as? UINavigationController, let controller = navController.root as? RankingViewController {
            controller.rootDelegate = rootDelegate
        }
        
        if let navController = segue.destination as? UINavigationController, let controller = navController.root as? CollaborateViewController {
            controller.rootDelegate = rootDelegate
        }
        
        if let navController = segue.destination as? UINavigationController, let controller = navController.root as? WebViewController, let url = sender as? String {
            controller.rootDelegate = rootDelegate
            controller.url = url
        }
        
    }
    
}

extension HomeViewController {
    
    // MARK: - Map functions
    
    private func addUserLocationPin() {
        if let location = userPresenter.getUserLocation {
            mapView.addAnnotation(withType: .user, location: location)
        } else {
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.5) {
                self.addUserLocationPin()
            }
        }
    }
    
    private func centerOnUserLocation(shouldUseSafeHouse: Bool = false) {
        func animateTo(location: CLLocationCoordinate2D) {
            mapView.animate(toZoom: 15)
            mapView.animate(toLocation: location)
        }
        #if targetEnvironment(simulator)
        if let location = userPresenter.getUserLocation {
            animateTo(location: location)
        } else {
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.5) {
                self.centerOnUserLocation()
            }
        }
        #else
        if geoPresenter.locationAvailable {
            if let location = userPresenter.getUserLocation {
                animateTo(location: location)
            } else {
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.5) {
                    self.centerOnUserLocation()
                }
            }
        } else {
            if shouldUseSafeHouse, let location = userPresenter.getSafehouseLocation {
                animateTo(location: location)
            }
            if !disabledUserLocationAlertShown {
                let okButton = (R.localizable.showLocationConfig(), UIAlertAction.Style.default, {
                    self.homePresenter.openSMODSettings()
                    self.disabledUserLocationAlertShown = true
                })
                let cancelButton = (R.localizable.later(), UIAlertAction.Style.cancel, {
                    self.disabledUserLocationAlertShown = true
                })
                SEAlert.show(title: R.localizable.advice(), message: R.localizable.enableLocationDialog(), buttons: cancelButton, okButton)
            } else {
                showSnackBar(withAlert: R.localizable.disabledLocationAlert(), duration: 5)
            }
        }
        #endif
    }
    
    private func postNearbyService() {
        if userPresenter.getUserHasEnteredSafehouse {
            let mapRegion = mapView.projection.visibleRegion()
            let currentBounds = BoundsModel()
            currentBounds.topLeft = PointModel(latitude: mapRegion.farLeft.latitude, longitude: mapRegion.farLeft.longitude)
            currentBounds.topRight = PointModel(latitude: mapRegion.farRight.latitude, longitude: mapRegion.farRight.longitude)
            currentBounds.bottomLeft = PointModel(latitude: mapRegion.nearLeft.latitude, longitude: mapRegion.nearLeft.longitude)
            currentBounds.bottomRight = PointModel(latitude: mapRegion.nearRight.latitude, longitude: mapRegion.nearRight.longitude)
            geoPresenter.postGeoNearby(data: currentBounds)
        } else {
            if !disabledSafehouseAlertShown {
                SEAlert.show(title: R.localizable.advice(), message: R.localizable.missingSafehouse(), buttons: (R.localizable.ok(), UIAlertAction.Style.default, {
                    self.disabledSafehouseAlertShown = true
                }))
            }
        }
    }
    
    private func showUsersLocations() {
        mapView.clear()
        addUserLocationPin()
        for user in users {
            guard let latestLocation = user.latestPosition?.position?.coordinates?.location else { return }
            let type: COBEATMarkerViewType = user.isQuarantined == true ? .infected : .collaborator
            let converted = CLLocationCoordinate2D(latitude: latestLocation.coordinate.latitude, longitude: latestLocation.coordinate.longitude)
            mapView.addAnnotation(withType: type, location: converted)
        }
    }
    
    private func showLegendLabels() {
        legendCollaboratorsLabel.text = "\(users.count) \(R.localizable.collaboratorsPin())"
        legendInfectedLabel.text = "\(totalInfected) \(R.localizable.infectedPin())"
    }
    
}

extension HomeViewController {
    
    // MARK: - Scroll table functions
    
    private func tableDefaultYPosition() -> CGFloat {
        return self.view.frame.height - HomeFlapTableViewCell.height
    }
    
    private func handleFlapTap () {
        if tableTopViewTopConstraint.constant == tableDefaultYPosition() {
            tableTopViewTopConstraint.constant = 0
        } else {
            tableTopViewTopConstraint.constant = tableDefaultYPosition()
        }
        isTapHandled = true
        UIView.animate(withDuration: 0.3, animations: {
            self.view.layoutIfNeeded()
        }) { (_) in
            self.isTapHandled = false
        }
    }
    
    private func handleFlapScroll(_ scrollView: UIScrollView) {
        if !isTapHandled {
            let contentOffset =  scrollView.contentOffset.y - oldContentOffset.y
            if contentOffset > 0 && scrollView.contentOffset.y > 0 {
                if (tableTopViewTopConstraint.constant > 0 ) {
                    tableTopViewTopConstraint.constant -= contentOffset
                    scrollView.contentOffset.y -= contentOffset
                }
            }
            if contentOffset < 0 && scrollView.contentOffset.y < 0 {
                if tableTopViewTopConstraint.constant < tableDefaultYPosition() {
                    tableTopViewTopConstraint.constant -= contentOffset
                } else {
                    tableTopViewTopConstraint.constant = tableDefaultYPosition()
                }
            }
            oldContentOffset = scrollView.contentOffset
        }
    }
    
    private func getUnfilledTableSpace() -> CGFloat {
        var filled: CGFloat = 0
        for item in homePresenter.infoTableDatasource {
            switch item {
            case .flap:
                filled += HomeFlapTableViewCell.height
            case .info, .telephones:
                filled += HomeUtilsTableViewCell.height
            case .news:
                filled += HomeNewsTableViewCell.height
            case .empty:
                filled += 0
            }
        }
        return (view.frame.height - filled) > 0 ? (view.frame.height - filled) : 0
    }
    
}

extension HomeViewController {
    
    // MARK: - Notification functions
    
    private func showInformationView() {
        informationLabel.text = notifications.first(where: { $0.isHeader })?.title
        if !notificationClosed && userPresenter.getUserHasEnteredSafehouse {
            informationContentHeight.constant = 60
            UIView.animate(withDuration: 0.3, animations: {
                self.view.layoutIfNeeded()
            }, completion: { (_) in
                self.informationContentWidth.constant = self.view.frame.width * 0.85
                UIView.animate(withDuration: 0.3, animations: {
                    self.view.layoutIfNeeded()
                }, completion: { (_) in
                    self.informationLabel.isHidden = false
                    self.informationButton.isHidden = false
                    UIView.animate(withDuration: 0.2) {
                        self.informationLabel.alpha = 1
                        self.informationButton.alpha = 1
                    }
                })
            })
        }
    }
    
    private func hideInformationView() {
        UIView.animate(withDuration: 0.2, animations: {
            self.informationLabel.alpha = 0
            self.informationButton.alpha = 0
        }, completion: { (_) in
            self.informationContentWidth.constant = 0
            UIView.animate(withDuration: 0.3, animations: {
                self.view.layoutIfNeeded()
            }) { (_) in
                self.informationLabel.isHidden = true
                self.informationButton.isHidden = true
                self.notificationClosed = true
                self.informationContentHeight.constant = 0
                UIView.animate(withDuration: 0.3) {
                    self.view.layoutIfNeeded()
                }
            }
        })
    }
    
}

extension HomeViewController {
    
    // MARK: - Feed functions
    
    private func getCityFeed() {
        placePresenter.getCityFeed()
    }
}

extension HomeViewController {
    
    // MARK: - SafeHouse functions
    
    private func configureWithSafehouse() {
        tableView.isHidden = false
        safeHouseContentViewBottom.constant = -safeHouseContentView.frame.height
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
    }
    
    private func configureWithoutSafehouse() {
        setupColaborateButton()
        tableView.isHidden = true
        if safeHouseContentViewBottom.constant == -safeHouseContentView.frame.height {
            safeHouseContentViewBottom.constant = 0
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
            }
        }
    }
    
}

extension HomeViewController: UITableViewDelegate, UITableViewDataSource {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        handleFlapScroll(scrollView)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return homePresenter.infoTableDatasource.count
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return CGFloat.zero
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return nil
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return nil
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = homePresenter.infoTableDatasource[indexPath.row]
        switch item {
        case .flap:
            let cell = tableView.reusableCell(withIdentifier: HomeFlapTableViewCell.self)
            cell.setup(withTitle: flapNotificationText)
            return cell
        case .info:
            let cell = tableView.reusableCell(withIdentifier: HomeUtilsTableViewCell.self)
            cell.setupWithType(item)
            cell.onTopContentTapped {
                self.showQuiz()
            }
            cell.onBottomContentTapped {
                self.shareAPP()
            }
            return cell
        case .telephones:
            let cell = tableView.reusableCell(withIdentifier: HomeUtilsTableViewCell.self)
            cell.onTopContentTapped {
                self.callSympton()
            }
            cell.onBottomContentTapped {
                self.callReport()
            }
            cell.setupWithType(item)
            return cell
        case .news:
            let cell = tableView.reusableCell(withIdentifier: HomeNewsTableViewCell.self)
            cell.setup(withFeed: feed)
            cell.onShowFeedDetail { (feed) in
                self.showFeedDetail(feed)
            }
            return cell
        case .empty:
            let cell = tableView.reusableCell(withIdentifier: HomeEmptyTableViewCell.self)
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let item = homePresenter.infoTableDatasource[indexPath.row]
        switch item {
        case .flap:
            return HomeFlapTableViewCell.height
        case .info, .telephones:
            return HomeUtilsTableViewCell.height
        case .news:
            return HomeNewsTableViewCell.height
        case .empty:
            return getUnfilledTableSpace()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
        let item = homePresenter.infoTableDatasource[indexPath.row]
        switch item {
        case .flap:
            handleFlapTap()
        case .info, .telephones, .empty:
            break
        case .news:
            break
        }
        return false
    }
}

extension HomeViewController: GMSMapViewDelegate, GMSAutocompleteViewControllerDelegate {
    
    // GMSMapViewDelegate
    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        postNearbyService()
    }
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        return false
    }
    
    func didTapMyLocationButton(for mapView: GMSMapView) -> Bool {
        centerOnUserLocation()
        return false
    }
    
    // GMSAutocompleteViewControllerDelegate
    
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        safeHouseTextfield.text = place.formattedAddress
        set(address: place)
        setupColaborateButton()
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        debugLog("GMSAutocompleteViewControllerDelegateError: \(error.localizedDescription)")
    }
    
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
}

extension HomeViewController: BasePresenterDelegate {
    
    func initService(type: DLService) {
        if type != .postGeoNearby {
            LoadingView.show()
        }
    }
    
    func finishService(type: DLService) {
        LoadingView.dismiss()
    }
    
    func onError(message: String?) {
        LoadingView.dismiss()
        SEAlert.show(title: R.localizable.ups(), message: message ?? "", buttons: (R.localizable.ok(), UIAlertAction.Style.default, { }))
    }
    
}

extension HomeViewController: HomePresenterDelegate {
    
    
}

extension HomeViewController: GeoPresenterDelegate {
    
    func onPostGeo(nearby users: [UserModel], notifications: [NotificationModel]) {
        self.users = users
        self.notifications = notifications
        showUsersLocations()
        showInformationView()
        showLegendLabels()
        reloadTableWithNotifications()
    }
    
    func onPostGeo(newPoint position: PositionModel) {
        
    }
    
}

extension HomeViewController: UserPresenterDelegate {
    
    func onPutUserMe() {
        setupUserSafeHouse()
        postNearbyService()
    }
    
}

extension HomeViewController: PlacePresenterDelegate {
    
    func onGetCity(feed: [CityFeedModel]) {
        self.feed = feed
        tableView.reloadData()
    }
    
}
