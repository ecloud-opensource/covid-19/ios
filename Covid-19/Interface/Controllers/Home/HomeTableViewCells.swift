//
//  HomeTableViewCells.swift
//  Covid-19
//
//  Created by Gonzalo Sanchez on 02/04/2020.
//  Copyright © 2020 eCloudSolutions. All rights reserved.
//

import UIKit
import MarqueeLabel

class HomeFlapTableViewCell: UITableViewCell {
    
    @IBOutlet private var mainContent: UIView!
    @IBOutlet private var flapView: UIView!
    @IBOutlet var informationLabel: MarqueeLabel!
    
    static let height: CGFloat = 90
    private var marqueeTimer: Timer?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    // MARK: - Functions
    
    func setup(withTitle title: String) {
        informationLabel.text = title
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + .milliseconds(500)) {
            self.forceScrollIfNeeded()
        }
    }
    
    // MARK: - Private unctions
    
    private func setup() {
        mainContent.rounded(10, withColor: .cobeatGrayBackground)
        mainContent.addShadow(color: UIColor.black.withAlphaComponent(0.5))
        flapView.roundBorders(value: flapView.frame.height/2)
        flapView.backgroundColor = UIColor.black.withAlphaComponent(0.2)
        informationLabel.set(font: .gothamRoundedBook(15), andColor: .darkGray)
        informationLabel.type = .continuous
        informationLabel.speed = .duration(15)
        informationLabel.animationCurve = .linear
        informationLabel.fadeLength = 10.0
    }
    
    private func forceScrollIfNeeded() {
        if informationLabel.labelShouldScroll() {
            informationLabel.triggerScrollStart()
        }
    }
}

class HomeEmptyTableViewCell: UITableViewCell {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    // MARK: - Private unctions
    
    private func setup() {
        contentView.backgroundColor = .cobeatGrayBackground
    }
    
}

class HomeUtilsTableViewCell: UITableViewCell {
    
    static let height: CGFloat = 300
    
    @IBOutlet private var mainContent: UIView!
    @IBOutlet private var topButtonContent: UIControl!
    @IBOutlet private var topButtonTitle: UILabel!
    @IBOutlet private var topButtonSubttle: UILabel!
    @IBOutlet private var bottomButtonContent: UIControl!
    @IBOutlet private var bottomButtonTitle: UILabel!
    @IBOutlet private var bottomButtonSubttle: UILabel!
    
    private var topContentTapped: (()->Void)?
    private var bottomContentTapped: (()->Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        contentView.layoutIfNeeded()
        setup()
    }
    
    // MARK: - Functions
    
    func setupWithType(_ type: HomeUtilsCellType) {
        contentView.backgroundColor = type.contentBackgroundColor
        mainContent.backgroundColor = type.mainBackgroundColor
        topButtonContent.backgroundColor = type.topBackgroundColor
        topButtonTitle.text = type.topTitle
        topButtonTitle.font = type.topTitleFont
        topButtonTitle.textColor = type.topTitleColor
        topButtonSubttle.text = type.topSubtitle
        topButtonSubttle.font = type.topSubtitleFont
        topButtonSubttle.textColor = type.topSubtitleColor
        bottomButtonContent.backgroundColor = type.bottomBackgroundColor
        bottomButtonTitle.text = type.bottomTitle
        bottomButtonTitle.font = type.bottomTitleFont
        bottomButtonTitle.textColor = type.bottomTitleColor
        bottomButtonSubttle.text = type.bottomSubtitle
        bottomButtonSubttle.font = type.bottomSubtitleFont
        bottomButtonSubttle.textColor = type.bottomSubtitleColor
    }
    
    func onTopContentTapped(_ callback: @escaping ()->Void) {
        self.topContentTapped = callback
    }
    
    func onBottomContentTapped(_ callback: @escaping ()->Void) {
        self.bottomContentTapped = callback
    }
    
    // MARK: - Private functions
    
    private func setup() {
        mainContent.rounded(corners: [.topLeft, .topRight], cornerRadii: CGSize(width: 15, height: 15))
        topButtonContent.roundBorders(value: 15)
        topButtonContent.addShadow(color: .black, radious: 3, opacity: 0.2)
        bottomButtonContent.roundBorders(value: 15)
        bottomButtonContent.addShadow(color: .black, radious: 3, opacity: 0.2)
    }
    
    // MARK: - IBAction
    
    @IBAction private func onTopButtonTapped () {
        topContentTapped?()
    }
    
    @IBAction private func onBoBttomuttonTapped () {
        bottomContentTapped?()
    }
    
}

class HomeNewsTableViewCell: UITableViewCell, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    static let height: CGFloat = 300
    
    @IBOutlet private var mainContent: UIView!
    @IBOutlet private var collectionView: UICollectionView!
    private var showFeedDetail: ((CityFeedModel)->Void)?
    private var feed = [CityFeedModel]() {
        didSet {
            collectionView.reloadData()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        contentView.layoutIfNeeded()
        setup()
    }
    
    // MARK: - Functions
    
    func onShowFeedDetail(_ callback: @escaping (CityFeedModel)->Void) {
        self.showFeedDetail = callback
    }
    
    func setup(withFeed feed: [CityFeedModel]) {
        self.feed = feed
    }
    
    // MARK: - Private functions
    
    private func setup() {
        contentView.backgroundColor =  HomeUtilsCellType.news.contentBackgroundColor
        mainContent.rounded(corners: [.topLeft, .topRight], cornerRadii: CGSize(width: 15, height: 15))
        mainContent.backgroundColor = HomeUtilsCellType.news.mainBackgroundColor
    }
    
    // MARK: - UICollectionViewDataSource
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return feed.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.reusableCell(withIdentifier: HomeNewsCollectionViewCell.self, for: indexPath)
        cell.setup(withFeed: feed[indexPath.row])
        return cell
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    // MARK: - UICollectionViewDelegateFlowLayout
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let height = collectionView.frame.height
        return CGSize(width: height * 0.75, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 20
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize.zero
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        return CGSize.zero
    }
    
    // MARK: - UICollectionViewDelegate
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
    
    func collectionView(_ collectionView: UICollectionView, shouldHighlightItemAt indexPath: IndexPath) -> Bool {
        showFeedDetail?(feed[indexPath.row])
        return false
    }
    
}

class HomeNewsCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet private var topContent: UIView!
    @IBOutlet private var bottomContent: UIView!
    @IBOutlet private var image: UIImageView!
    @IBOutlet private var titleLabel: UILabel!
    @IBOutlet private var subtitleLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        contentView.layoutIfNeeded()
    }
    
    func setup(withFeed feed: CityFeedModel) {
        image.setImage(withURL: feed.image ?? "")
        titleLabel.text = feed.title
        subtitleLabel.text = feed.info
    }
    
    private func setup() {
        contentView.roundBorders(value: 5)
        topContent.backgroundColor = .darkGray
        bottomContent.backgroundColor = .white
        titleLabel.set(font: .gothamRoundedBook(12), andColor: UIColor.black)
        subtitleLabel.set(font: .gothamRoundedBook(10), andColor: UIColor.black.withAlphaComponent(0.8))
    }

}
