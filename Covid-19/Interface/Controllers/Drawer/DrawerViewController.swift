//
//  DrawerViewController.swift
//  Covid-19
//
//  Created by Gonzalo Sanchez on 30/03/2020.
//  Copyright © 2020 eCloudSolutions. All rights reserved.
//

import UIKit
import SwiftExtension

class DrawerViewController: COBEATBaseViewController {
    
    @IBOutlet private var navbarContainer: UIView!
    @IBOutlet private var closeButton: UIButton!
    @IBOutlet private var logoImage: UIImageView!
    @IBOutlet private var tableView: UITableView!
    
    var rootDelegate: RootViewControllerDelegate?
    private var placePresenter = PlacePresenter<DrawerViewController>()
    private var drawerItems: [DrawerSectionType] = [.header, .profile, .links, .ranking, .collaborate, .terms, .footer]
    private var currentCity = CityModel() {
        didSet {
            tableView.reloadData()
        }
    }
    private var currentCityDetail: String {
        guard let fellows = currentCity.fellows, let infected = currentCity.areInfected else {
            return "0 \(R.localizable.drawerHeaderCollaborators()) | 0 \(R.localizable.drawerHeaderInfected())"
        }
        return "\(fellows) \(R.localizable.drawerHeaderCollaborators()) | \(infected) \(R.localizable.drawerHeaderInfected())"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.setNavigationBarTranslucent()
        tableView.reloadData()
        placePresenter.getCitySingle()
    }
    
    override func navigationControllerDismissTapped(_ sender: AnyObject) {
        rootDelegate?.closeDrawer(nil)
    }
    
    private func setup() {
        placePresenter.attach(view: self)
        view.backgroundColor = UIColor.white.withAlphaComponent(0.9)
        navbarContainer.backgroundColor = .cobeatPrimaryPrimary
        navbarContainer.rounded(corners: [.bottomLeft, .bottomRight], cornerRadii: CGSize(width: 20, height: 20))
        closeButton.setImage(R.image.iconClose()?.with(color: .white), for: .normal)
        logoImage.image = R.image.cobeatWideWhite()
    }
    
}

extension DrawerViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return drawerItems.count
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return nil
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return DrawerFooterTableViewCell.height
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let cell = tableView.reusableCell(withIdentifier: DrawerFooterTableViewCell.self)
        return cell
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = drawerItems[indexPath.row]
        switch item {
        case .header:
            let cell = tableView.reusableCell(withIdentifier: DrawerHeaderTableViewCell.self)
            cell.setup(withTitle: currentCityDetail)
            return cell
        case .profile, .links, .ranking, .collaborate, .terms:
            let cell = tableView.reusableCell(withIdentifier: DrawerItemTableViewCell.self)
            cell.setup(withType: drawerItems[indexPath.row])
            return cell
        case .footer:
            let cell = UITableViewCell()
            cell.backgroundColor = .clear
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let item = drawerItems[indexPath.row]
        switch item {
        case .header:
            return DrawerHeaderTableViewCell.height
        case .profile, .links, .ranking, .collaborate, .terms:
            return DrawerItemTableViewCell.height
        case .footer:
            let used = DrawerHeaderTableViewCell.height + (DrawerItemTableViewCell.height * 5) + DrawerFooterTableViewCell.height
            let available = tableView.frame.height - used
            return available > 0 ? available : 0
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
        let item = drawerItems[indexPath.row]
        switch item {
        case .header, .footer:
            return false
        case .profile, .links, .ranking, .collaborate, .terms:
            rootDelegate?.presentSectionFromHome(item)
            return false
        }
    }
}

extension DrawerViewController: PlacePresenterDelegate {
    
    func initService(type: DLService) {
        if type == .getCitySingle {
            
        }
    }
    
    func finishService(type: DLService) {
        if type == .getCitySingle {
            
        }
    }
    
    func onError(message: String?) {
        LoadingView.dismiss()
    }

    func onGetCitySingle(city: CityModel) {
        currentCity = city
    }

}


