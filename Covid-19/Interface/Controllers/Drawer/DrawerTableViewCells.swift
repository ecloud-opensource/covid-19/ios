//
//  DrawerTableViewCells.swift
//  Covid-19
//
//  Created by Gonzalo Sanchez on 05/04/2020.
//  Copyright © 2020 eCloudSolutions. All rights reserved.
//

import UIKit

class DrawerHeaderTableViewCell: UITableViewCell {
    
    @IBOutlet private var mainContent: UIView!
    @IBOutlet private var titleLabel: UILabel!
    @IBOutlet private var subtitleLabel: UILabel!
    
    static let height: CGFloat = 130
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    // MARK: - Functions
    
    func setup(withTitle title: String) {
        subtitleLabel.text = title
    }
    
    // MARK: - Private unctions
    
    private func setup() {
        mainContent.rounded(10, withColor: .cobeatLightBrownBackground)
        mainContent.addShadow(color: .black, radious: 3, opacity: 0.2)
        titleLabel.set(font: .gothamRoundedMedium(11), andColor: UIColor.white.withAlphaComponent(0.5))
        titleLabel.text = R.localizable.yourCityTitle()
        subtitleLabel.set(font: .gothamRoundedBook(15), andColor: .white)
    }

}

class DrawerItemTableViewCell: UITableViewCell {
    
    static let height: CGFloat = 50
    
    @IBOutlet private var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    // MARK: - Functions
    
    func setup(withType type: DrawerSectionType) {
        titleLabel.text = type.title
    }
    
    // MARK: - Private unctions
    
    private func setup() {
        titleLabel.set(font: .gothamRoundedBook(17), andColor: .cobeatPrimaryPrimary)
    }

}

class DrawerFooterTableViewCell: UITableViewCell {
    
    static let height: CGFloat = 190
    
    @IBOutlet private var mainContent: UIView!
    @IBOutlet private var titleLabel: UILabel!
    @IBOutlet private var subtitleLabel: UILabel!
    @IBOutlet private var logo: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        contentView.layoutIfNeeded()
        setup()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    // MARK: - Private unctions
    
    private func setup() {
        mainContent.rounded(corners: [.topLeft, .topRight], cornerRadii: CGSize(width: 20, height: 20))
        mainContent.backgroundColor = .cobeatPrimaryPrimary
        titleLabel.set(font: .gothamRoundedLight(13), andColor: .white)
        titleLabel.text = R.localizable.madeWithCollaboration()
        subtitleLabel.set(font: .gothamRoundedBook(13), andColor: .white)
        subtitleLabel.text = R.localizable.weHelpEachOther()
        logo.image = R.image.logoFull()
    }

}
