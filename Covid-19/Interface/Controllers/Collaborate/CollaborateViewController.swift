//
//  CollaborateViewController.swift
//  Covid-19
//
//  Created by Gonzalo Sanchez on 05/04/2020.
//  Copyright © 2020 eCloudSolutions. All rights reserved.
//

import UIKit
import SwiftExtension

class CollaborateViewController: COBEATBaseViewController {
    
    @IBOutlet private var sendButton: UIButton!
    @IBOutlet private var collaborateButtonContent: UIControl!
    @IBOutlet private var collaborateButtonTitle: UILabel!
    @IBOutlet private var collaborateButtonSubttle: UILabel!
    @IBOutlet var firstnameTitleLabel: UILabel!
    @IBOutlet var firstnameTextfield: TextfieldWithError!
    @IBOutlet var lastnameTitleLabel: UILabel!
    @IBOutlet var lastnameTextfield: TextfieldWithError!
    @IBOutlet var emailTitleLabel: UILabel!
    @IBOutlet var emailTextfield: TextfieldWithError!
    @IBOutlet var messageTitleLabel: UILabel!
    @IBOutlet var messageTextview: UITextView!
    @IBOutlet var messageTextviewCountLabel: UILabel!

    private let kDisabledTextviewColor = UIColor.lightGray.withAlphaComponent(0.5)
    private let kMessageMaxCharacters = 140
    private var data = ContactModel()

    var rootDelegate: RootViewControllerDelegate?
    private var userPresenter = UserPresenter<CollaborateViewController>()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationItem.setCenterTitle(R.localizable.collaborate(), color: .cobeatPrimaryPrimary)
        navigationItem.setCloseLeftButton(viewController: self, color: .cobeatPrimaryPrimary)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    // MARK: - Setup functions
    
    private func setup() {
        userPresenter.attach(view: self)
        sendButton.setTitle(R.localizable.continue(), color: .white, font: UIFont.gothamRoundedBook(17))
        sendButton.rounded(10, withColor: .cobeatPrimaryPrimary)
        sendButton.addShadow(color: .black, opacity: 0.3)
        collaborateButtonContent.roundBorders(value: 10)
        collaborateButtonContent.addShadow(color: .black, radious: 3, opacity: 0.2)
        collaborateButtonContent.backgroundColor = .cobeatLightBrownBackground
        collaborateButtonTitle.set(font: .gothamRoundedBook(15), andColor: .white)
        collaborateButtonTitle.text = R.localizable.wantToUseTheSystem()
        collaborateButtonSubttle.set(font: .gothamRoundedMedium(12), andColor: UIColor.white.withAlphaComponent(0.5))
        collaborateButtonSubttle.text = R.localizable.writeToUsNow().uppercased()
        firstnameTitleLabel.font = .gothamRoundedBook(10)
        firstnameTitleLabel.textColor = .lightGray
        firstnameTitleLabel.text = R.localizable.firstnameTitle()
        firstnameTextfield.font = .gothamRoundedBook(16)
        firstnameTextfield.textColor = .darkGray
        firstnameTextfield.underlinedForError(color: .lightGray)
        firstnameTextfield.placeholder = R.localizable.firstnamePlaceholder()
        firstnameTextfield.autocorrectionType = .no
        lastnameTitleLabel.font = .gothamRoundedBook(10)
        lastnameTitleLabel.textColor = .lightGray
        lastnameTitleLabel.text = R.localizable.lastnameTitle()
        lastnameTextfield.font = .gothamRoundedBook(16)
        lastnameTextfield.textColor = .darkGray
        lastnameTextfield.underlinedForError(color: .lightGray)
        lastnameTextfield.placeholder = R.localizable.lastnamePlaceholder()
        lastnameTextfield.autocorrectionType = .no
        emailTitleLabel.font = .gothamRoundedBook(10)
        emailTitleLabel.textColor = .lightGray
        emailTitleLabel.text = R.localizable.emailTitle()
        emailTextfield.font = .gothamRoundedBook(16)
        emailTextfield.textColor = .darkGray
        emailTextfield.underlinedForError(color: .lightGray)
        emailTextfield.placeholder = R.localizable.emailPlaceholder()
        emailTextfield.autocorrectionType = .no
        messageTitleLabel.font = .gothamRoundedBook(10)
        messageTitleLabel.textColor = .lightGray
        messageTitleLabel.text = R.localizable.message()
        messageTextview.bordered(1, lineColor: .lightGray)
        messageTextview.font = .gothamRoundedBook(16)
        messageTextview.textColor = .darkGray
        messageTextview.roundBorders(value: 10)
        messageTextview.autocorrectionType = .no
        messageTextview.textContainerInset = UIEdgeInsets(top: 10, left: 5, bottom: 10, right: 5)
        setMessageTextviewToolbar()
        defaultMessageTextview()
        messageTextviewCountLabel.set(font: .gothamRoundedBook(10), andColor: .lightGray)
        defaultFormData()
        checkButton()
    }
    
    private func checkButton() {
        sendButton.isEnabled = data.validated
        sendButton.backgroundColor = data.validated ? .cobeatPrimaryPrimary : UIColor.cobeatPrimaryPrimary.withAlphaComponent(0.3)
    }
    
    private func defaultFormData() {
        data = ContactModel()
        data.firstName = userPresenter.editingUser?.firstName
        data.lastName = userPresenter.editingUser?.lastName
        data.email = userPresenter.editingUser?.email
        firstnameTextfield.text = userPresenter.editingUser?.firstName
        lastnameTextfield.text = userPresenter.editingUser?.lastName
        emailTextfield.text = userPresenter.editingUser?.email
        messageTextview.text = nil
        defaultMessageTextview()
        checkButton()
    }

    // MARK: - Override
    
    override func keyboardWillShow(_ keyboardHeight: CGFloat) {
        self.view.frame.origin.y = 0
        self.view.frame.origin.y -= keyboardHeight/4.5
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
    }
    
    override func keyboardWillHide(_ keyboardHeight: CGFloat) {
        self.view.frame.origin.y = 0
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
    }
    
    override func closeRightButton() {
        self.dismiss(animated: true)
    }
    
    // MARK: - IBAction

    @IBAction private func onContinueTapped() {
        userPresenter.postUserContact(data: data)
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
    }
    
}

extension CollaborateViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case firstnameTextfield:
            data.firstName = firstnameTextfield.text
            lastnameTextfield.becomeFirstResponder()
        case lastnameTextfield:
            data.lastName = lastnameTextfield.text
            emailTextfield.becomeFirstResponder()
        case emailTextfield:
            data.email = emailTextfield.text
            messageTextview.becomeFirstResponder()
        default:
            break
        }
        checkButton()
        return true
    }
    
}

extension CollaborateViewController: UITextViewDelegate {
    
    private func defaultMessageTextview() {
        if messageTextview.text.isEmpty {
            messageTextview.text = R.localizable.insertYourMessage()
            messageTextview.textColor = kDisabledTextviewColor
            messageTextviewCountLabel.text = "0/\(kMessageMaxCharacters)"
        } else {
            messageTextview.textColor = .darkGray
            messageTextviewCountLabel.text = "\(messageTextview.text.count)/\(kMessageMaxCharacters)"
        }
    }
        
    private func setMessageTextviewToolbar() {
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = .darkGray
        toolBar.sizeToFit()
        var rightButton: UIBarButtonItem
        rightButton = UIBarButtonItem(title: R.localizable.ok(), style: .plain, target: self, action: #selector(textviewDismissed))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        toolBar.isUserInteractionEnabled = true
        toolBar.setItems([spaceButton, rightButton], animated: false)
        messageTextview.inputAccessoryView = toolBar
    }
    
    @objc private func textviewDismissed() {
        data.message = messageTextview.text
        messageTextview.resignFirstResponder()
        checkButton()
    }
    
    // UITextViewDelegate
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if messageTextview.textColor == kDisabledTextviewColor {
            textView.text = nil
            textView.textColor = .darkGray
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if messageTextview.text.isEmpty {
            messageTextview.text = R.localizable.insertYourMessage()
            messageTextview.textColor = kDisabledTextviewColor
            messageTextviewCountLabel.text = "0/\(kMessageMaxCharacters)"
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let newText = ((textView.text ?? "") as NSString).replacingCharacters(in: range, with: text)
        let textCount = newText.count
        if textCount <= kMessageMaxCharacters {
            messageTextviewCountLabel.text = "\(textCount)/\(kMessageMaxCharacters)"
        }
        return textCount <= kMessageMaxCharacters
    }
    
}

extension CollaborateViewController: UserPresenterDelegate {
    
    func initService(type: DLService) {
        LoadingView.show()
    }
    
    func finishService(type: DLService) {
        LoadingView.dismiss()
    }
    
    func onError(message: String?) {
        LoadingView.dismiss()
        SEAlert.show(title: R.localizable.ups(), message: message ?? "", buttons: (R.localizable.ok(), UIAlertAction.Style.default, { }))
    }
    
    func onPostUserContact() {
        defaultFormData()
        SEAlert.show(title: R.localizable.congratulations(), message: R.localizable.messageSent(), buttons: (R.localizable.ok(), UIAlertAction.Style.default, { }))
    }
}
