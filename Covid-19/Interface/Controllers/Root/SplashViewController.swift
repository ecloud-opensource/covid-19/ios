//
//  SplashViewController.swift
//  Covid-19
//
//  Created by Gonzalo Sanchez on 30/03/2020.
//  Copyright © 2020 eCloudSolutions. All rights reserved.
//

import UIKit
import SwiftExtension
import CoreLocation

class SplashViewController: COBEATBaseViewController, SEObserver {
    
    var id = "splash-controller-observer"

    @IBOutlet private var splashImage: UIImageView!
    @IBOutlet private var versionLabel: UILabel!
    @IBOutlet private var splashActivity: UIActivityIndicatorView!
    
    var rootDelegate: RootViewControllerDelegate?
    private var splashPresenter = SplashPresenter<SplashViewController>()
    private var settingPresenter = SettingPresenter<SplashViewController>()
    private var userPresenter = UserPresenter<SplashViewController>()
    private var locationManagerAuthorizedFinished = false

    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        #warning("Fetch remote config")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        FirebaseMessagingMananger.shared.setup()
        SEMessaging.instance.deviceToken.add(observer: self, on: deviceToken)
        SEMessaging.instance.requestAuthorization()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - Functions
    
    // MARK: - Private functions
    
    private func setup() {
        
        splashPresenter.attach(view: self)
        settingPresenter.attach(view: self)
        userPresenter.attach(view: self)

        splashActivity.color = .white
        versionLabel.textColor = .white
        view.backgroundColor = .cobeatPrimaryPrimary
        
        #if DEBUG
        if let bundleVersion = Bundle.bundleVersion, let bundleBuild = Bundle.bundleBuild {
            versionLabel.text = "Version \(bundleVersion) (\(bundleBuild))"
        }
        #else
        versionLabel.text = nil
        #endif
    }
    
    private func getSettings() {
        settingPresenter.getSettings()
    }
    
    private func getTerms() {
        settingPresenter.getTermsAndConditions()
    }
    private func getUser() {
        UserManager.instance.user == nil ? startAPP() : userPresenter.getUserMe()
    }
    
    private func startAPP() {
        rootDelegate?.appStarting()
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
    }
    
}

extension SplashViewController: BasePresenterDelegate {
    func onError(message: String?) {
        startAPP()
    }
}

extension SplashViewController: SplashPresenterDelegate {
    
}

extension SplashViewController: SettingPresenterDelegate {
    
    func onSettings() {
        getTerms()
    }
    
    func onTermsAndConditions(url: String) {
        UIDefault.termsURL.set(value: url)
        getUser()
    }
}

extension SplashViewController: UserPresenterDelegate {
    func onGetUserMe() {
        startAPP()
    }
}


extension SplashViewController {
    
    private func deviceToken(token: Data?, error: NSError?) {
        OperationQueue.main.addOperation {
            SELocation.instance.status.add(observer: self, on: self.locationManagerAuthorized)
            SELocation.instance.requestAuthorization(authorization: .authorizedAlways)
        }
    }
}

extension SplashViewController {
    func locationManagerAuthorized(status: CLAuthorizationStatus) {
        if !locationManagerAuthorizedFinished {
            locationManagerAuthorizedFinished = true
            SELocation.instance.startUpdatingLocation()
            getSettings()
        }
    }
}


