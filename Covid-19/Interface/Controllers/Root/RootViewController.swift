//
//  RootViewController.swift
//  Covid-19
//
//  Created by Gonzalo Sanchez on 30/03/2020.
//  Copyright © 2020 eCloudSolutions. All rights reserved.
//

import UIKit
import SwiftExtension
import KYDrawerController

protocol RootViewControllerDelegate {
    func appStarting()
    func openDrawer(_ completion: (() -> ())?)
    func closeDrawer(_ completion: (() -> ())?)
    func showHome(_ completion: (() -> ())?)
    func presentSectionFromHome(_ section: DrawerSectionType)
}

class RootViewController: COBEATBaseViewController {
    
    static var sharedInstance: RootViewController?
    
    private let kDrawerAnimationDuration: TimeInterval = 0.25
    private let drawerViewController = KYDrawerController(drawerDirection: .left, drawerWidth: UIScreen.main.bounds.width)
    private var _drawerMenuNavigationController: UINavigationController?
    private var _authNavigationController: UINavigationController?
    private var _homeNavigationController: UINavigationController?

    private var rootPresenter = RootPresenter<RootViewController>()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    // MARK: - Functions
    
    private func setup() {
        rootPresenter.attach(view: self)
        rootPresenter.readUser()
        RootViewController.sharedInstance = self
    }
    
    private func configureDrawer() {
        setDrawer(centerController: initCenterNavigationController())
        drawerViewController.drawerAnimationDuration = kDrawerAnimationDuration
        drawerViewController.drawerViewController = drawerMenuNavController
        drawerViewController.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        UIApplication.shared.changeRoot(withViewController: drawerViewController)
    }
    
    private func setDrawer(centerController center: UINavigationController) {
        drawerViewController.mainViewController = center
    }
    
    private func dismissModalControllers() {
        drawerViewController.mainViewController?.dismissNavigationController()
    }
    
    func initCenterNavigationController() -> UINavigationController {
        return rootPresenter.logged ? homeNavController : authNavController
    }
    
    func checkDeeplinkFlow() {
        
    }

    // MARK: - Navigation
    
    private func showSplash() {
        performSegue(withIdentifier: R.segue.rootViewController.splash.identifier, sender: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let controller = segue.destination as? SplashViewController {
            controller.rootDelegate = self
        }
    }
    
}

extension RootViewController {
    
    //drawerMenuViewController
    private var drawerMenuNavController: UINavigationController {
        if _drawerMenuNavigationController == nil {
            _drawerMenuNavigationController = UINavigationController.initFrom(R.storyboard.drawer.name)
            if let root = _drawerMenuNavigationController?.root as? DrawerViewController {
                root.rootDelegate = self
            }
        }
        return _drawerMenuNavigationController!
    }
    
    //authViewController
    private var authNavController: UINavigationController {
        if _authNavigationController == nil {
            _authNavigationController = UINavigationController.initFrom(R.storyboard.auth.name)
            if let root = _authNavigationController?.root as? AuthViewController {
                root.rootDelegate = self
            }
        }
        return _authNavigationController!
    }
    
    //homeViewController
    private var homeNavController: UINavigationController {
        if _homeNavigationController == nil {
            _homeNavigationController = UINavigationController.initFrom(R.storyboard.home.name)
            if let root = _homeNavigationController?.root as? HomeViewController {
                root.rootDelegate = self
            }
        }
        return _homeNavigationController!
    }
    
}

extension RootViewController: RootViewControllerDelegate {
    
    func appStarting() {
        configureDrawer()
    }
    
    func openDrawer(_ completion: (() -> ())? = nil) {
        drawerViewController.setDrawerState(.opened, animated: true)
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + kDrawerAnimationDuration) {
            completion?()
        }
    }
    
    func closeDrawer(_ completion: (() -> ())? = nil) {
        drawerViewController.setDrawerState(.closed, animated: true)
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + kDrawerAnimationDuration) {
            completion?()
        }
    }
    
    func showHome(_ completion: (() -> ())?) {
        setDrawer(centerController: initCenterNavigationController())
        closeDrawer {
            completion?()
        }
    }
    
    func presentSectionFromHome(_ section: DrawerSectionType) {
        closeDrawer {
            if let vc = self.homeNavController.root as? HomeViewController {
                vc.present(section: section)
            }
        }
    }
    
}

extension RootViewController: RootPresenterDelegate {
    
    func startLoading() {
        
    }
    
    func finishLoading() {
        
    }
    
    func onError(message: String?) {
        showSplash()
    }
    
    func onReadUser() {
        showSplash()
    }
    
}
